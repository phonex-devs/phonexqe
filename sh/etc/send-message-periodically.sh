#!/bin/bash
#Usage
# ./send-message-periodically.sh [serialNumberOfDevice] [xCoordinateOfSendButton] [yCoordinateOfSendButton] [delayBetweenMessagesInSeconds]
for (( i=1; ; i++ ))
do
	text=`date +%s`
	adb -s $1 shell input text $text
	adb -s $1 shell input tap $2 $3
	sleep "$4"s
done

