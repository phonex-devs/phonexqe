#!/bin/bash
#Usage
echo "\$1 device1 serial number"
echo "\$2 device2 serial number"
echo "\$3 xCoordinateOfCallButton"
echo "\$4 yCoordinateOfCallButton"
echo "\$5 xStartSwipeToAnswer"
echo "\$6 yStartSwipeToAnswer"
echo "\$7 xEndSwipeToAnswer"
echo "\$8 yEndSwipeToAnswer"
echo "\$9 xTerminateCallButton"
echo "\$10 yTerminateCallButton"
for (( i=1; ; i++ ))
do
	#tapping call button
	adb -s "$1" shell input tap "$3" "$4"
	#dialing
	sleep 4
	#answering
	adb -s "$2" shell input swipe "$5" "$6" "$7" "$8"
	#calling
	sleep 4s
	#terminating
	adb -s "$1" shell input tap 535 1550
	#waiting for another run
	sleep 4s
done

