#!/bin/bash           

while read line           
do  
    JUST_KEY=`echo $line | grep "<string name=\"" | cut -d'>' -f1 | cut -d'=' -f2 | tr -d ' '`
    if [[ ! -z "$JUST_KEY" ]]; then
	    cd ../values-sk
	    ANO_PANI_BEATA=`grep -r "$JUST_KEY" * | tr -d ' '`
	    #echo $ANO_PANI_BEATA
	    if [[ -z "$ANO_PANI_BEATA" ]]; then
	   	echo $line
	    fi
    fi
done < $1
