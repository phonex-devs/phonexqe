#!/bin/bash
#Usage
for (( i=1; ; i++ ))
do
	#tap status switcher position
	adb -s $1 shell input tap $2 $3
	sleep 1s
	#tap away status
	adb -s $1 shell input tap $4 $5
	sleep 2s
	#tap status switcher
	adb -s $1 shell input tap $2 $3
	sleep 1s
	#tap online
	adb -s $1 shell input tap $2 $3
	sleep 2s
	#tap status switcher
	adb -s $1 shell input tap $2 $3
	sleep 1s
	#tap invisible
	adb -s $1 shell input tap $6 $7
	sleep 2s
	#tap status switcher
	adb -s $1 shell input tap $2 $3
        sleep 1s
	#tap online
	adb -s $1 shell input tap $2 $3
	sleep 2s
done

