#!/bin/bash

rm "$1".logcat > /dev/null 2>&1
eval $ANDROID_HOME/platform-tools/adb -s $1 logcat -c
eval $ANDROID_HOME/platform-tools/adb -s $1 logcat -v time > $1.logcat &
tail -f $1.logcat
