#!/bin/bash
if [ "$#" -ne 2 ]; then
    echo "ERROR: Illegal number of parameters"
    echo "Usage: ./logcatOnDeviceCI.sh [deviceUDID] [pathToStoreLog]"
    exit -1
fi

rm "$2/$1".logcat > /dev/null 2>&1
$ANDROID_HOME/platform-tools/adb -s $1 logcat -c
$ANDROID_HOME/platform-tools/adb -s $1 logcat -v time > $2/$1.logcat &
