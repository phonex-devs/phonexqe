if [ "$#" -ne 2 ]; then
    echo "ERROR: Illegal number of parameters"
    echo "Usage: ./reinstallPhoneX [deviceName] [pathToAPKToInstall]"
    echo "Possible devices options: XPERIA, HTC, TABLET, S4, PETO_SAMSUNG"
    exit -1
fi

export XPERIA=CB511ZVJ7Q
export HTC=0123456789ABCDEF
export TABLET1=WAWJMUB37H
export TABLET2=WAWJM1JT87
export S4=971a1b97
export PETO_SAMSUNG=3df6b8a4
export LENKA_SAMSUNG=0b78e2c0
export MIRO_SAMSUNG_S2=0019eadb30fd5e
export EMULATOR1=emulator-5554
export NEXUS7=0a36d01a

$ANDROID_HOME/platform-tools/adb kill-server
eval $ANDROID_HOME/platform-tools/adb -s \${$1} uninstall net.phonex
eval $ANDROID_HOME/platform-tools/adb -s \${$1} install $2

