#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "ERROR: Illegal number of parameters"
    echo "Usage: ./logcatOnDevice.sh [deviceName]"
    echo "Possible devices options: XPERIA, HTC, TABLET, S4, PETO_SAMSUNG, EMULATOR"
    exit -1
fi

export XPERIA=CB511ZVJ7Q
export HTC=0123456789ABCDEF
export TABLET1=WAWJMUB37H
export TABLET2=WAWJM1JT87
export S4=971a1b97
export PETO_SAMSUNG=3df6b8a4
export EMULATOR1=emulator-5554
export MIRO_SAMSUNG_S2=0019eadb30fd5e
export NEXUS7=0a36d01a

rm "$1".logcat > /dev/null 2>&1
#pkill adb > /dev/null 2>&1
eval echo \${$1}
eval $ANDROID_HOME/platform-tools/adb -s \${$1} logcat -c
eval $ANDROID_HOME/platform-tools/adb -s \${$1} logcat -v time > $1.logcat &
eval tail -f $1.logcat
