#!/bin/bash

# constants
APK_PATH='/app/build/outputs/apk/'
APK_FILE_NAME='app-release'
APK_EXTENSION='.apk'
PROGUARD_MAPPING_PATH='/app/build/outputs/mapping/release/mapping.txt'
EXPECTED_APK_SIZE='13948326'

# START SCRIPT
echo 'Following script is going to verify released PhoneX APK!'
read -p 'Press ENTER to continue or Ctrl+C for exit...'
echo ''

#cleaning
echo ''
echo 'CLEANING...'
rm /tmp/$APK_FILE_NAME$APK_EXTENSION > /dev/null 2>&1
rm -rf /tmp/$APK_FILE_NAME > /dev/null 2>&1
rm /tmp/extractedStrings.txt > /dev/null 2>&1

# checking whether all required tools are installed
which apktool &>/dev/null
[ $? -eq 0 ] || echo "ERROR: apktool should be runable from cmd. See https://code.google.com/p/android-apktool/wiki/Install for more info"

which git &>/dev/null
[ $? -eq 0 ] || echo "ERROR: git should be installed"

# read variables values
read -p 'Enter the path to the root of PhoneX Android git repository: ' REPOSITORY_PATH
read -p 'Enter GIT tag/branch to check against: ' GIT_TAG

echo ''
echo 'Going to work with following variables:'
echo ''
echo 'Repository path: ' $REPOSITORY_PATH
echo 'Git tag: ' $GIT_TAG
echo 'APK which will be under reverse engineering test: ' $REPOSITORY_PATH$APK_PATH$APK_FILE_NAME$APK_EXTENSION
echo 'Proguard mapping file taken into consideration: ' $REPOSITORY_PATH$PROGUARD_MAPPING_PATH
read -p 'Press ENTER to continue or Ctrl+C for exit...'
echo ''

# check whether entered repository is directory
if [[ ! -d $REPOSITORY_PATH ]]; then
 echo 'The entered path to repository must be an existing directory!'
 exit -1
fi

# find all string constants used in logging, save them in file
# run python script for that
echo ''
echo FINDING STRING CONSTANTS USED IN LOGGING...
cd $REPOSITORY_PATH
#git fetch origin > /dev/null 2>&1
#git checkout $GIT_TAG > /dev/null 2>&1

cd pytools
find ../app/src/ -name "*.java" -exec python logExtract.py -v 2 -o /tmp/extractedStrings.txt {} \;

# decompile given APK in /tmp directory and check it for string in $LOG_STRING
# if the proguard was succesful, it should not be there
echo ''
echo DECOMPILING APK...
cd /tmp
cp $REPOSITORY_PATH$APK_PATH$APK_FILE_NAME$APK_EXTENSION .
apktool d $APK_FILE_NAME$APK_EXTENSION
cd $APK_FILE_NAME

echo ''
echo CHECKING THE APK SMALI FILES FOR STRING CONSTANTS
echo ''
# check APK for containing messages
# 1. it will looks for the string retrieved above
# 2. if some file contains such string then it will check whether
#    it contains also method name retrieved from proguard mapping file
ERROR=false
COUNTER=1
TOTAL=`cat /tmp/extractedStrings.txt | wc -l`
LOGGING_CLASS_PROGUARD_NAME=`grep "net.phonex.util.Log ->" $REPOSITORY_PATH$PROGUARD_MAPPING_PATH | cut -d'>' -f2 | cut -d' ' -f2 | cut -d':' -f1`
LOGGING_CLASS_PROGUARD_NAME=$(echo "$LOGGING_CLASS_PROGUARD_NAME" | tr . //)
while read line
do
    FILES_AND_LINE=`grep -r -A 20 "const-string v[0-9]\\{1,\\}, \"$line"`
    JUST_FILE_NAMES=`grep -rl "const-string v[0-9]\\{1,\\}, \"$line"`
    NUMBER_OF_LINES=`echo -n "$FILES_AND_LINE" | wc -l`
    if [[ $NUMBER_OF_LINES -ne 0 ]]; then
	if [[ "$FILES_AND_LINE" == *"$LOGGING_CLASS_PROGUARD_NAME"* ]]; then
		ERROR=true
		echo "Log entry: \"$line\" was not removed successfully!"
		echo "In files: \"$JUST_FILE_NAMES\""
	fi
    fi
    echo -ne "Total: $TOTAL/Current: $COUNTER\r"
    COUNTER=$((COUNTER + 1))
done < /tmp/extractedStrings.txt
if [[ $ERROR == true ]]; then
    echo 'ERROR: Proguard was not successfull in removing all log entries!'
    exit -1
fi

# check android:installLocation is set to internalOnly
NUMBER_OF_LINES=`grep 'android:installLocation="internalOnly"' AndroidManifest.xml  | wc -l`
if [[ $NUMBER_OF_LINES != 1 ]]; then
    echo 'ERROR: android:installLocation is not set to internalOnly!'
    exit -1;
fi

# check for android:debuggable is either not set or set to false
NUMBER_OF_LINES=`grep 'android:debuggable="true"' AndroidManifest.xml  | wc -l`
if [[ $NUMBER_OF_LINES != 0 ]]; then
    echo 'ERROR: android:debuggable can not be set to true'
    exit -1;
fi

# check whether the size of APK is not too big, which would indicate a possible problem
cd /tmp
ACTUAL_APK_SIZE=`wc -c $APK_FILE_NAME$APK_EXTENSION | cut -d' ' -f1`
if [[ $EXPECTED_APK_SIZE < $ACTUAL_APK_SIZE ]]; then
    echo 'ERROR: the released APK has unexpected size.'
    echo "Expected: no more than $EXPECTED_APK_SIZE"
    echo "but was: $ACTUAL_APK_SIZE"
    exit -1;
fi

echo ''
echo 'RESULTS:'
echo 'Log entries were removed - SUCCESS'
echo 'android:installLocation was set to internalOnly - SUCCESS'
echo 'andorid:debuggable is not set - SUCCESS'
echo 'size of the APK is allright - SUCCESS'
