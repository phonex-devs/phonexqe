#!/bin/bash

QOS="./qos.sh"
echo -n "Shaping WIFI to "

case "$1" in

    GPRS)
	echo "GRPRS"
	$QOS stop > /dev/null 2>&1
	$QOS start 80kbit 20kbit 200ms 40ms 200ms 40ms 5%
	;;

    EDGE)
	echo "EDGE"
	$QOS stop > /dev/null 2>&1
	$QOS start 200kbit 260kbit 120ms 40ms 120ms 40ms 5%
	;;

    HDSPA)
	echo "HDSPA"
	$QOS stop > /dev/null 2>&1
	$QOS start 2400kbit 2400kbit 100ms 100ms 100ms 100ms 5%
	;;

    LTE)
	echo "LTE"
	;;

    FULL)
	echo "FULL"
	$QOS stop > /dev/null 2>&1
	;;

    DISABLED)
	echo "DISABLED"
	$QOS stop > /dev/null 2>&1
	$QOS start 1kbit 1kbit 5000ms 5000ms 5000ms 5000ms 5%
	;;

    *)
        pwd=$(pwd)
        echo "Usage: set-qos.sh {GPRS|EDGE|HDSPA|LTE|FULL|DISABLED}"
        ;;

esac

exit 0
