#!/bin/bash
for (( i=1; ; i++ ))
do
   #SLEEP=`shuf -i 1-10 -n 1`
   QUALITY=`shuf -i 1-4 -n 1`
   case "$QUALITY" in

     1) 
      service wifi_access_point stop
      service wifi_access_point start
      ./set-qos.sh GPRS
      ;;

     2)
      service wifi_access_point stop
      service wifi_access_point start
      ./set-qos.sh EDGE
      ;;
     
     3)
      service wifi_access_point stop
      service wifi_access_point start
      ./set-qos.sh HDSPA
      ;;

     4)
      service wifi_access_point stop
      service wifi_access_point start
      ./set-qos.sh DISABLED
      ;;

    esac

    echo "Sleeping for 5 minutes."
    sleep 5m
done
