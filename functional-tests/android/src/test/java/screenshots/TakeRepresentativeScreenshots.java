package screenshots;

import io.appium.java_client.android.AndroidDriver;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import net.phonex.tests.AbstractTest;
import static net.phonex.tests.AbstractTest.DEVICE_1_NAME;
import static net.phonex.tests.AbstractTest.DEVICE_1_URL;
import net.phonex.tests.fragments.call.CallFragment;
import net.phonex.tests.fragments.chat.ChatFragment;
import net.phonex.tests.utils.AsyncUtils;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author jhuska
 */
public class TakeRepresentativeScreenshots extends AbstractTest {

    private Future<String> secondDeviceFT = null;

    private static final String DEMO_1_ACCCOUNT_LOGIN = "demo01";
    private static final String DEMO_2_ACCCOUNT_LOGIN = "demo02";

    private static final String DEMO_1_ACCCOUNT_PASSWORD = "aaaaaaaa1";
    private static final String DEMO_2_ACCCOUNT_PASSWORD = "aaaaaaaa1";

    private void setUpDevices() throws Exception {
        secondDeviceFT = AsyncUtils.executeAsyncTask(new Callable<String>() {
            public String call() throws Exception {
                set_up_device2();
                loginIntoApp(device2, DEMO_2_ACCCOUNT_LOGIN, DEMO_2_ACCCOUNT_PASSWORD);
                waitUntilUserIsLoggedIn(device2);
                return "done";
            }
        });

        takeScreenshotsOnDevice1Process = startTakingOfScreenshots(DEVICE_1_NAME);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", DEVICE_1_NAME);
        setCapability(capabilities, DEVICE_1_NAME, "udid");
        setCapability(capabilities, DEVICE_1_NAME, "platformVersion");
        capabilities.setCapability("app", getApp1().getAbsolutePath());
        setCommonCapabilities(capabilities);
        device1 = new AndroidDriver(new URL(DEVICE_1_URL), capabilities);
        setReinstalledA44(device1);
        waitUntillAppIsInitializedA44(device1);
        sendFileToRemoteDevice(DEVICE_1_NAME, LocalisedFiles.getPathToLocalisedFileForMessagingScreen());
        sendFileToRemoteDevice(DEVICE_1_NAME, LocalisedFiles.FILE_TRANSFER_SCREEN_PHOTO_1);
        sendFileToRemoteDevice(DEVICE_1_NAME, LocalisedFiles.FILE_TRANSFER_SCREEN_PHOTO_2);
    }

    @Test
    public void takeScreenshots() throws Exception {
        setUpDevices();
        //baam INTRODUCTION
        introFragment.continueToLoginA44(device1);
        loginFragment.waitUntillActiveA44(device1);
        //baam LOGIN
        loginFragment.loginA44(device1, DEMO_1_ACCCOUNT_LOGIN, DEMO_1_ACCCOUNT_PASSWORD);
        contactListFragment.waitUntilContactsAreVisibleA44(device1, TIMEOUT_LOGIN);
        secondDeviceFT.get();
        //baam CONTACT LIST
        contactListFragment.gotoSendMessageA44(device1, DEMO_2_ACCCOUNT_LOGIN);
        chatFragment.waitUntilVisibleA44(device1);
        String greeting = LocalisedStrings.get("greeting");
        chatFragment.sendMessageA44(device1, greeting);
        contactListFragment.waitUntilNewMessageReceived(device2, DEMO_1_ACCCOUNT_LOGIN, ChatFragment.TIMEOUT_MESSAGE_SENT);
        contactListFragment.tapNewMessageImage(device2, DEMO_1_ACCCOUNT_LOGIN);
        chatFragment.waitUntilVisible(device2);
        String fine = LocalisedStrings.get("fine");
        chatFragment.sendMessage(device2, fine);
        chatFragment.waitUntilMessageWithContentReceived(fine, device1);
        String willWeSign = LocalisedStrings.get("willWeSign");
        chatFragment.sendMessageA44(device1, willWeSign);
        chatFragment.waitUntilMessageWithContentReceived(willWeSign, device2);
        String yesSend = LocalisedStrings.get("yesSend");
        chatFragment.sendMessage(device2, yesSend);
        chatFragment.waitUntilMessageWithContentReceived(yesSend, device1);
        String hereYouAre = LocalisedStrings.get("hereYouAre");
        System.out.println(hereYouAre);
        chatFragment.sendMessageA44(device1, hereYouAre);
        chatFragment.waitUntilMessageWithContentReceived(hereYouAre, device2);
        chatFragment.tapAttachmentButton(device1);
        filePickerFragment.waitUntilVisibleA44(device1);
        filePickerFragment.gotoDownloadA44(device1);
        filePickerFragment.waitUntilSortOrderIsVisibleA44(device1);
        filePickerFragment.selectFile(LocalisedFiles.getFileName(), device1);
        chatFragment.goBackToContactList(device2);
        filePickerFragment.tapSendButtonA44(device1);
        contactListFragment.waitUntilNewMessageReceived(device2, DEMO_1_ACCCOUNT_LOGIN, TIMEOUT_FILE_SENT);
        contactListFragment.tapNewMessageImage(device2, DEMO_1_ACCCOUNT_LOGIN);
        chatFragment.waitUntilVisible(device2);
        chatFragment.waitUntilFileIsDownloaded(device2, LocalisedFiles.getFileName());
        //baam MESSAGING
        chatFragment.goBackToContactList(device1);
        contactListFragment.waitUntilContactsAreVisibleA44(device1, TIMEOUT_SCREEN_TRANSITION);
        contactListFragment.dialContactA44(device1, DEMO_2_ACCCOUNT_LOGIN);
        callFragment.waitUntillCallIsIncoming(device2);
        callFragment.answerCall(device2, DEVICE_2_NAME);
        sasDialogFragment.confirmA44(device1);
        sasDialogFragment.confirm(device2);
        Thread.sleep(60000);
        //baam CALLS
        callFragment.hungUpA44(device1);
        contactListFragment.waitUntilContactsAreVisibleA44(device1, CallFragment.TIMOUT_CALL_IS_HANG_UP);
        appTabsMenuFragment.gotoMessagesTab(device1);
        messagesFragment.removeConversationThread(device1, 0);
        appTabsMenuFragment.goToContactsTab(device1);
        contactListFragment.gotoSendMessageA44(device1, DEMO_2_ACCCOUNT_LOGIN);
        String sendingPrototype = LocalisedStrings.get("sendingPrototype");
        chatFragment.sendMessageA44(device1, sendingPrototype);
        chatFragment.waitUntilMessageWithContentReceived(sendingPrototype, device2);
        chatFragment.tapAttachmentButton(device1);
        filePickerFragment.waitUntilVisibleA44(device1);
        filePickerFragment.gotoDownloadA44(device1);
        filePickerFragment.selectFile(LocalisedFiles.getJustFileName(LocalisedFiles.FILE_TRANSFER_SCREEN_PHOTO_1), device1);
        filePickerFragment.selectFile(LocalisedFiles.getJustFileName(LocalisedFiles.FILE_TRANSFER_SCREEN_PHOTO_2), device1);
        filePickerFragment.tapSendButtonA44(device1);
        contactListFragment.waitUntilNewMessageReceived(device2, DEMO_1_ACCCOUNT_LOGIN, TIMEOUT_FILE_SENT);
        contactListFragment.tapNewMessageImage(device2, DEMO_1_ACCCOUNT_LOGIN);
        chatFragment.sendMessage(device2, LocalisedStrings.get("thanksIWillGoThrough"));
        //baam FILE TRANSFER
    }
}
