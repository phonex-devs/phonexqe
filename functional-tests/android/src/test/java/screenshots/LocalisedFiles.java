package screenshots;

import java.io.File;

/**
 *
 * @author jhuska
 */
public class LocalisedFiles {

    private static final String CZ = "01a-smlouva-navrh.doc";
    private static final String EN = "01a-contract-draft.doc";
    
    public static final String FILE_TRANSFER_SCREEN_PHOTO_1 = "res/carPrototype/01a.jpg";
    public static final String FILE_TRANSFER_SCREEN_PHOTO_2 = "res/carPrototype/01b.jpg";

    public static String getFileName() {
        String fileName = null;
        if (LocalisedStrings.LOCALE == null) {
            throw new IllegalStateException("You have to run tests with system property stringsLocale.");
        }
        switch (LocalisedStrings.LOCALE.toLowerCase()) {
            case "cz":
                fileName = CZ;
                break;
            case "en":
                fileName = EN;
                break;
        }
        return fileName;
    }

    public static String getPathToLocalisedFileForMessagingScreen() {
        return new File("res/localizedFiles/" + getFileName()).getAbsolutePath();
    }
    
    public static String getJustFileName(String fullPath) {
        return fullPath.substring(fullPath.lastIndexOf("/") + 1);
    }
}