package screenshots;

import java.io.File;
import java.io.FileReader;
import net.phonex.tests.AbstractTest;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author jhuska
 */
public class LocalisedStrings {

    public static final String LOCALE = System.getProperty("stringsLocale");
    
    public static JSONObject getStringsJSON() throws Exception {
        if(LOCALE == null) {
            throw new IllegalStateException("You have to run tests with system property stringsLocale.");
        }
        JSONParser parser = new JSONParser();
        String particularStrings = AbstractTest.STRINGS_DIR + File.separator + LOCALE + ".json";
        Object obj = parser.parse(new FileReader(particularStrings));
        return (JSONObject) obj;
    }

    public static String get(String property) throws Exception {
        return getStringsJSON().get(property).toString();
    }

}
