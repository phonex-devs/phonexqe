package net.phonex.tests;

import static net.phonex.tests.MessagingTest.TIMEOUT_MESSAGE_RECEIVED;
import net.phonex.tests.fragments.chat.ChatFragment.Message;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author jhuska
 */
public class BroadcastMessageTest extends AbstractTestTwoDevices {
    
    @Test
    public void testBroadcastTextMessage() {
        selectContactsAndTapBroadcastMessage();
        
        final String message = MessagingTest.getRandomString();
        Message toSend = new Message();
        toSend.setText(message);
        broadcastMessageFragment.typeMessage(device1, message);
        broadcastMessageFragment.tapSendButton(device1);
        
        contactListFragment.waitUntilNewMessageReceived(device2, QA_LOGIN_DEVICE_1, TIMEOUT_MESSAGE_RECEIVED);
        contactListFragment.tapNewMessageImage(device2, QA_LOGIN_DEVICE_1);
        Assert.assertTrue("The sent message was not received correctly!",
                chatFragment.getMessages(device2).contains(toSend));
    }
    
    @Test
    public void testBroadcastFileMessage() throws Exception {
        sendFileToRemoteDevice(DEVICE_1_NAME);
        
        syncFileTransfersKeys(device1);
        syncFileTransfersKeys(device2);
        Thread.sleep(TIMEOUT_FILE_TRANSFER_KEYS_SYNCED);
        
        selectContactsAndTapBroadcastMessage();
        broadcastMessageFragment.tapAttachmentIcon(device1);
        filePickerFragment.waitUntilVisible(device1);
        filePickerFragment.gotoDownload(device1);
        filePickerFragment.selectFile(FILE_TO_SEND_1_NAME, device1);
        filePickerFragment.tapSendButton(device1);

        contactListFragment.waitUntilNewMessageReceived(device2, QA_LOGIN_DEVICE_1, TIMEOUT_FILE_SENT);
        contactListFragment.tapNewMessageImage(device2, QA_LOGIN_DEVICE_1);
        chatFragment.waitUntilVisible(device2);
        chatFragment.waitUntilFileIsDownloaded(device2, FILE_TO_SEND_1_NAME);
    }
    
    private void selectContactsAndTapBroadcastMessage() {
        contactListFragment.longTapOverContact(device1, QA_LOGIN_DEVICE_2);
        contactListFragment.selectContacts(device1, QA_LOGIN_DEVICE_3);
        contactListFragment.tapBroadcastMessageIcon(device1);
        broadcastMessageFragment.waitUntilVisible(device1);
    }
}