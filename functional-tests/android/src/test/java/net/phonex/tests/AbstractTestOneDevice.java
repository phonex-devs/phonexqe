package net.phonex.tests;

import org.junit.Before;

/**
 *
 * @author jhuska
 */
public class AbstractTestOneDevice extends AbstractTest {
    
    @Before
    public void set_up() throws Exception {
        set_up_device1();
        loginIntoApp(device1, QA_LOGIN_DEVICE_1);
        waitUntilUserIsLoggedIn(device1);
    }
}