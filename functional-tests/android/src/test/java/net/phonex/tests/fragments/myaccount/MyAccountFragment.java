package net.phonex.tests.fragments.myaccount;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import java.util.List;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class MyAccountFragment {

    private static final int TIMEOUT_FRAGMENT_SHOWN = 3000;
    private static final int TIMEOUT_APP_IS_RECOVERING = 25000;
    private static final String FRAGMENT_TITLE = "Account name:";
    private static final String APP_IS_RECOVERING_STATE = "Recovering application state, please wait.";
    private static final String RESTART_APP_TXT_VIEW = "Restart application";
    private static final String SYNC_KEYS_TXT_VIEW = "Synchronize keys";
    private static final String CHANGE_PASSWD_TXT_VIEW = "Change password";
    
    public void tapRestartApp(AndroidDriver device) {
        ElementUtils.getTextView(device, RESTART_APP_TXT_VIEW).click();
    }

    public void tapSyncKeys(AndroidDriver device) {
        ElementUtils.getTextView(device, SYNC_KEYS_TXT_VIEW).click();
    }

    public void tapChangePassword(AndroidDriver device) {
        ElementUtils.getTextView(device, CHANGE_PASSWD_TXT_VIEW).click();
    }

    public void waitUntilAppIsRecoveringState(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Dialog recovering app state... was not shown in time!",
                device, TIMEOUT_APP_IS_RECOVERING, new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        List<WebElement> textViews = ElementUtils.getTextViews(device);
                        return !textViews.isEmpty()
                        && textViews.get(1).getText().equals(APP_IS_RECOVERING_STATE);
                    }
                });
    }
    
    public void waitUntilVisible(final AndroidDriver device, int timeout) {
        WaitUtils.waitUntilPredicateHolds("My account screen was not shown in time!", device, timeout,
                new Predicate<WebDriver>() {

                    public boolean apply(WebDriver t) {
                        List<WebElement> textViews = ElementUtils.getTextViews(device);
                        return !textViews.isEmpty()
                        && textViews.get(0).getText().contains(FRAGMENT_TITLE);

                    }
                });
    }

    public void waitUntilVisible(final AndroidDriver device) {
        waitUntilVisible(device, TIMEOUT_FRAGMENT_SHOWN);
    }
}
