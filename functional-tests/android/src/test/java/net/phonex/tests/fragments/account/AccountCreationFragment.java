package net.phonex.tests.fragments.account;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import java.util.List;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class AccountCreationFragment {

    public static final int TIMEOUT_CAPTCHA_GENERATED = 10000;
    public static final int TIMEOUT_SHOWN_TIME = 4000;
    
    public static final String SCREEN_HEADER = "Account creation";

    public void typeUserName(String userName, AndroidDriver device) {
        ElementUtils.getEditTexts(device).get(0).sendKeys(userName);
    }

    public void typeCaptcha(String captcha, final AndroidDriver device) throws Exception {
        waitUntillProgressBarIsGone(device);
        ElementUtils.getEditTexts(device).get(1).sendKeys(captcha);
    }

    public void submit(AndroidDriver device) throws Exception {
        device.hideKeyboard();
        ElementUtils.getButtons(device).get(0).click();
    }

    public void waitTillActive(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Account creation screen was not shown in time!", 
                device, TIMEOUT_SHOWN_TIME, new Predicate<WebDriver>() {
            public boolean apply(WebDriver t) {
                List<WebElement> textViews = ElementUtils.getTextViews(device);
                return !textViews.isEmpty()
                        &&
                        textViews.get(0).getText().equals(SCREEN_HEADER);
            }
        }
        );
    }

    private void waitUntillProgressBarIsGone(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Progress bar did not disappear in time! Captcha was not generated in time!",
                device, TIMEOUT_CAPTCHA_GENERATED,
                new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return ElementUtils.getProgressBars(device).isEmpty()
                        || !ElementUtils.getProgressBars(device).get(0).isDisplayed();
                    }
                });
    }
}
