package net.phonex.tests.fragments.fileTransfer;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import java.util.List;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class FilePickerFragment {

    private static final int TIMEOUT_SHOWN = 4000;
    private static final int TIMEOUT_SUBFOLDER_SHOWN = 8000;

    private static final String FILE_PICKER_HEADER = "Choose a file";
    ;
    private static final String CAMERA = "Camera";
    private static final String PICTURES = "Pictures";
    private static final String MUSIC = "Music";
    private static final String DOWNLOAD = "Download";
    private static final String STORAGE = "Storage";
    
    private static final String FILE_LIMIT_TXT = "20.00 MB";
    
    private static final String DISABLE_SELECTION_RID = "disabler";
    private static final String WHERE_TO_CHOOSE_FROM_RID = "photo";
    private static final String SEND_BUTTON_FILE_PICKER_RID = "send";
    private static final String SORT_ORDER_RID = "sort_order";

    public void selectFile(String fileName, AndroidDriver device) {
        ElementUtils.getTextView(device, fileName).click();
    }

    public void tapSendButton(AndroidDriver device) {
        //this uglyness is needed because file picker invoked 
        //from broadcast message has under broadcast activity with its elements
        for(WebElement linearLayout : ElementUtils.getLinearLayouts(device)) {
            List<WebElement> textViews = linearLayout.findElements(By.className(ElementUtils.TEXT_VIEW));
            if(!textViews.isEmpty()) {
                if(textViews.get(0).getText().contains(FILE_LIMIT_TXT)) {
                    linearLayout.findElements(By.className(ElementUtils.IMAGE_BUTTON)).get(1).click();
                }
            }
        }
    }
    
    public void tapSendButtonA44(AndroidDriver device) {
        ElementUtils.findElementByResourceId(device, SEND_BUTTON_FILE_PICKER_RID).click();
    }

    public void gotoPictures(AndroidDriver device) {
        gotoFolder(PICTURES, device);
    }

    public void gotoMusic(AndroidDriver device) {
        gotoFolder(MUSIC, device);
    }

    public void gotoDownload(AndroidDriver device) {
        gotoFolder(DOWNLOAD, device);
    }
    
    public void gotoDownloadA44(AndroidDriver device) {
        List<WebElement> allOptions = ElementUtils.findElementsByResourceId(device, WHERE_TO_CHOOSE_FROM_RID);
        //it seems that Download option is always second from bottom, no matter how many options are there
        allOptions.get(allOptions.size() - 2).click();
    }
    
    public void waitUntilSortOrderIsVisibleA44(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds(
                "The sort order was not shown in time! The subfolder was not probably shown in time!", 
                device, TIMEOUT_SHOWN, new Predicate<WebDriver>() {

            @Override
            public boolean apply(WebDriver t) {
                return ElementUtils.findElementByResourceId(device, SORT_ORDER_RID) != null;
            }
        });
    }

    public void gotoStorage(AndroidDriver device) {
        gotoFolder(STORAGE, device);
    }

    public void gotoCamera(AndroidDriver device) {
        gotoFolder(CAMERA, device);
    }

    private void gotoFolder(String optionText, AndroidDriver device) {
        ElementUtils.getTextView(device, optionText).click();
        waitUntilSubfolderIsLoaded(device);
    }

    public void waitUntilSubfolderIsLoaded(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Subfolder was not shown in time!", device, TIMEOUT_SUBFOLDER_SHOWN,
                new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return !ElementUtils.getSpinners(device).isEmpty();
                    }
                });
    }

    @Deprecated
    public void waitUntilVisible(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("File picker screen was not shown in time!", device, TIMEOUT_SHOWN, new Predicate<WebDriver>() {

            public boolean apply(WebDriver t) {
                for (WebElement element : ElementUtils.getTextViews(device)) {
                    if (element.getText().equals(FILE_PICKER_HEADER)) {
                        return true;
                    }
                }
                return false;
            }
        });
    }
    
    public void waitUntilVisibleA44(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("File picker screen was not shown in time!", device, TIMEOUT_SHOWN, new Predicate<WebDriver>() {

            public boolean apply(WebDriver t) {
                return ElementUtils.findElementByResourceId(device, DISABLE_SELECTION_RID) != null;
            }
        });
    }
}