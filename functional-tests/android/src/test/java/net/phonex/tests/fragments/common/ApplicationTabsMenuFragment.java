package net.phonex.tests.fragments.common;

import io.appium.java_client.android.AndroidDriver;
import net.phonex.tests.utils.ElementUtils;

/**
 *
 * @author jhuska
 */
public class ApplicationTabsMenuFragment {
    
    private static final int CONTACTS_TAB_INTEX = 1;
    private static final int MESSAGES_TAB_INTEX = 2;
    private static final int NOTIFICATIONS_TAB_INTEX = 3;
    
    public void goToContactsTab(AndroidDriver device) {
        goToTab(CONTACTS_TAB_INTEX, device);
    }
    
    public void gotoMessagesTab(AndroidDriver device) {
        goToTab(MESSAGES_TAB_INTEX, device);
    }
    
    public void goToNotificationTab(AndroidDriver device) {
        goToTab(NOTIFICATIONS_TAB_INTEX, device);
    }
    
    private void goToTab(int tabIndex, AndroidDriver device) {
        ElementUtils.getImageViews(device).get(tabIndex).click();
    }
}
