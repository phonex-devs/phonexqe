package net.phonex.tests.utils;

import io.appium.java_client.android.AndroidDriver;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class ElementUtils {
    
    public static final String PHONEX_PACKAGE_RESOURCE_ID = "net.phonex:id/";
    
    public static final String RELATIVE_LAYOUT = "android.widget.RelativeLayout";
    public static final String FRAME_LAYOUT = "android.widget.FrameLayout";
    public static final String LINEAR_LAYOUT = "android.widget.LinearLayout";
    public static final String IMAGE_VIEW = "android.widget.ImageView";
    public static final String TEXT_VIEW = "android.widget.TextView";
    public static final String LIST_VIEW = "android.widget.ListView";
    public static final String IMAGE_BUTTON = "android.widget.ImageButton";
    public static final String BUTTON = "android.widget.Button";
    public static final String RECYCLER_VIEW = "android.support.v7.widget.RecyclerView";
    public static final String EDIT_TEXT = "android.widget.EditText";
    public static final String PROGRESS_BAR = "android.widget.ProgressBar";
    public static final String SPINNER = "android.widget.Spinner";
    public static final String VIEW_PAGER = "android.support.v4.view.ViewPager";
    public static final String MULTU_AUTO_COMPLETE_TEXT_VIEW = "android.widget.MultiAutoCompleteTextView";
    
    public static WebElement getLastTextView(AndroidDriver device) {
        List<WebElement> textViews = ElementUtils.getTextViews(device);
        return textViews.get(textViews.size() -1);
    }
    
    public static List<WebElement> getButtons(AndroidDriver device) {
        return device.findElements(By.className(BUTTON));
    }
    
    public static List<WebElement> getEditTexts(AndroidDriver device) {
        return device.findElements(By.className(EDIT_TEXT));
    }
    
    public static List<WebElement> getListViews(AndroidDriver device) {
        return device.findElements(By.className(LIST_VIEW));
    }
    
    public static List<WebElement> getRecyclerViews(AndroidDriver device) {
        return device.findElements(By.className(RECYCLER_VIEW));
    }
    
    public static List<WebElement> getRelativeLayouts(AndroidDriver device) {
        return device.findElements(By.className(RELATIVE_LAYOUT));
    }
    
    public static List<WebElement> getTextViews(AndroidDriver device) {
        return device.findElements(By.className(TEXT_VIEW));
    }
    
    public static List<WebElement> getImageButtons(AndroidDriver device) {
        return device.findElements(By.className(IMAGE_BUTTON));
    }
    
    public static List<WebElement> getImageViews(AndroidDriver device) {
        return device.findElements(By.className(IMAGE_VIEW));
    }
    
    public static List<WebElement> getProgressBars(AndroidDriver device) {
        return device.findElements(By.className(PROGRESS_BAR));
    }
    
    public static List<WebElement> getSpinners(AndroidDriver device) {
        return device.findElements(By.className(SPINNER));
    }
    
    public static List<WebElement> getLinearLayouts(AndroidDriver device) {
        return device.findElements(By.className(LINEAR_LAYOUT));
    }
    
    public static WebElement getTextView(AndroidDriver device, String contains) {
        return getElementContaining(getTextViews(device), contains);
    }
    
    public static WebElement getButton(AndroidDriver device, String contains) {
        return getElementContaining(getButtons(device), contains);
    }
    
    public static List<WebElement> getFrameLayouts(AndroidDriver device) {
        return device.findElements(By.className(FRAME_LAYOUT));
    }
    
    public static List<WebElement> getViewPagers(AndroidDriver device) {
        return device.findElements(By.className(VIEW_PAGER));
    }
    
    public static List<WebElement> getMultiAutoCompleteTextViews(AndroidDriver device) {
        return device.findElements(By.className(MULTU_AUTO_COMPLETE_TEXT_VIEW));
    }
    
    public static WebElement findElementByResourceId(AndroidDriver device, String resourceId) {
        List<WebElement> elements = findElementsByResourceId(device, resourceId);
        return !elements.isEmpty() ? elements.get(0) : null;
    }
    
    public static List<WebElement> findElementsByResourceId(AndroidDriver device, String resourceId) {
        return device.findElements(By.id(PHONEX_PACKAGE_RESOURCE_ID + resourceId));
    }
    
    private static WebElement getElementContaining(List<WebElement> list, 
            String contains) {
        WebElement result = null;
        for(WebElement element : list) {
            if(element.getText().contains(contains)) {
                result = element;
                break;
            }
        }
        return result;
    }
}