package net.phonex.tests;

import org.junit.Test;

/**
 *
 * @author jhuska
 */
public class RestartAppTest extends AbstractTestOneDevice {
    
    @Test
    public void test() throws Exception {
        hamburgerMenuFragment.show(device1);
        hamburgerMenuFragment.tapMyAccount(device1);
        myAccountFragment.waitUntilVisible(device1);
        myAccountFragment.tapRestartApp(device1);
        myAccountFragment.waitUntilAppIsRecoveringState(device1);
        contactListFragment.waitUntilVisible(device1, TIMEOUT_LOGIN);
    }
}