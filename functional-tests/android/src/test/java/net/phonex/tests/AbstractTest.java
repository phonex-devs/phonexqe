package net.phonex.tests;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import net.phonex.tests.fragments.common.ApplicationTabsMenuFragment;
import net.phonex.tests.fragments.call.CallFragment;
import net.phonex.tests.fragments.contactlist.ContactListFragment;
import net.phonex.tests.fragments.auth.LoginFragment;
import net.phonex.tests.fragments.notification.NotificationsFragment;
import net.phonex.tests.fragments.call.SASDialogFragment;
import net.phonex.tests.fragments.auth.IntroFragment;
import net.phonex.tests.fragments.chat.ChatFragment;
import net.phonex.tests.fragments.myaccount.MyAccountFragment;
import net.phonex.tests.fragments.account.AccountCreationFragment;
import net.phonex.tests.fragments.account.ChangeDefaultPasswordFragment;
import net.phonex.tests.fragments.account.SuccessDialogFragment;
import net.phonex.tests.fragments.common.HamburgerMenuFragment;
import net.phonex.tests.fragments.fileTransfer.FilePickerFragment;
import net.phonex.tests.fragments.chat.BroadcastMessageFragment;
import net.phonex.tests.fragments.chat.ForwardMessageFragment;
import net.phonex.tests.fragments.messages.MessagesFragment;
import net.phonex.tests.fragments.myaccount.NewPasswordFragment;
import static net.phonex.tests.utils.DevicesList.getDeviceProperty;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author jhuska
 */
public abstract class AbstractTest {

    public static final String APK_FILE_SYSTEM_PROPERTY_2 = "apkFile2";
    public static final String APK_FILE_SYSTEM_PROPERTY_1 = "apkFile1";
    public static final String DEVICE_1_NAME_SYS_PROPERTY = "device1Name";
    public static final String DEVICE_2_NAME_SYS_PROPERTY = "device2Name";
    public static final String DEVICE_1_URL_SYS_PROPERTY = "device1URL";
    public static final String DEVICE_2_URL_SYS_PROPERTY = "device2URL";

    public static final int TIMEOUT_LOGIN = 60000;
    public static final int TIMEOUT_APP_INIT = 10000;
    public static final int TIMEOUT_TAB_SWITCH = 3000;
    protected static final int TIMEOUT_FILE_SENT = 30000;
    protected static final int TIMEOUT_FILE_TRANSFER_KEYS_SYNCED = 60000;
    protected static final int TIMEOUT_SCREEN_TRANSITION = 4000;
    
    public static final String BUILD_DIR
            = System.getProperty("buildDir", "/tmp");
    public static final String RES_DIR = "res";
    public static final String QA_PASSWORD = "aaaaaaaa1";
    public static final String QA_LOGIN_DEVICE_1 = "jhuska2";
    public static final String QA_LOGIN_DEVICE_2 = "jhuska3";
    public static final String QA_LOGIN_DEVICE_3 = "jhuska4";
    public static final String FILE_TO_SEND_1_NAME = "01-a-photoForFileTransfer.jpg";

    public static final String DEVICE_LIST = "sh/device-list.json";
    public static final String STRINGS_DIR = "res/strings";
    public static final String SWIPE_TO_ANSWER_CALL_PROPERTY = "swipeToAnswerCall";
    public static final String DEVICE_1_NAME = System.getProperty(DEVICE_1_NAME_SYS_PROPERTY);
    public static final String DEVICE_2_NAME = System.getProperty(DEVICE_2_NAME_SYS_PROPERTY);
    public static final String DEVICE_1_URL = System.getProperty(DEVICE_1_URL_SYS_PROPERTY);
    public static final String DEVICE_2_URL = System.getProperty(DEVICE_2_URL_SYS_PROPERTY);

    protected AndroidDriver device1;
    protected AndroidDriver device2;

    protected IntroFragment introFragment = new IntroFragment();
    protected LoginFragment loginFragment = new LoginFragment();
    protected ContactListFragment contactListFragment = new ContactListFragment();
    protected CallFragment callFragment = new CallFragment();
    protected ApplicationTabsMenuFragment appTabsMenuFragment
            = new ApplicationTabsMenuFragment();
    protected SASDialogFragment sasDialogFragment = new SASDialogFragment();
    protected AccountCreationFragment accountCreationFragment = new AccountCreationFragment();
    protected ChangeDefaultPasswordFragment changeDefaultPasswdFragment = new ChangeDefaultPasswordFragment();
    protected SuccessDialogFragment successDialogFragment = new SuccessDialogFragment();
    protected NotificationsFragment notificationFragment = new NotificationsFragment();
    protected MyAccountFragment myAccountFragment = new MyAccountFragment();
    protected ChatFragment chatFragment = new ChatFragment();
    protected NewPasswordFragment newPasswordFragment = new NewPasswordFragment();
    protected FilePickerFragment filePickerFragment = new FilePickerFragment();
    protected BroadcastMessageFragment broadcastMessageFragment = new BroadcastMessageFragment();
    protected HamburgerMenuFragment hamburgerMenuFragment = new HamburgerMenuFragment();
    protected ForwardMessageFragment forwardMessageFragment = new ForwardMessageFragment();
    protected MessagesFragment messagesFragment = new MessagesFragment();
    
    private Boolean reinstalled = null;
    
    protected Process takeScreenshotsOnDevice1Process = null;
    protected Process takeScreenshotsOnDevice2Process = null;

    protected void set_up_device1() throws Exception {
        takeScreenshotsOnDevice1Process = startTakingOfScreenshots(DEVICE_1_NAME);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", DEVICE_1_NAME);
        setCapability(capabilities, DEVICE_1_NAME, "udid");
        setCapability(capabilities, DEVICE_1_NAME, "platformVersion");
        capabilities.setCapability("app", getApp1().getAbsolutePath());
        setCommonCapabilities(capabilities);
        device1 = new AndroidDriver(new URL(DEVICE_1_URL), capabilities);
        waitUntillAppIsInitialized(device1);
        setReinstalled(device1);
    }

    protected void set_up_device2() throws Exception {
        takeScreenshotsOnDevice2Process = startTakingOfScreenshots(DEVICE_2_NAME);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", DEVICE_2_NAME);
        setCapability(capabilities, DEVICE_2_NAME, "udid");
        setCapability(capabilities, DEVICE_2_NAME, "platformVersion");
        capabilities.setCapability("app", getApp2().getAbsolutePath());
        setCommonCapabilities(capabilities);
        device2 = new AndroidDriver(new URL(DEVICE_2_URL), capabilities);
        waitUntillAppIsInitialized(device2);
        setReinstalled(device2);
    }

    protected void setCommonCapabilities(DesiredCapabilities capabilities) {
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
        capabilities.setCapability("appPackage", "net.phonex");
        capabilities.setCapability("appActivity", ".ui.PhonexActivity");
    }

    protected void setCapability(DesiredCapabilities capabilities, String deviceName, String key)
            throws Exception {
        capabilities.setCapability(key, getDeviceProperty(deviceName, key));
    }

    protected File getApp2() {
        return new File(System.getProperty(APK_FILE_SYSTEM_PROPERTY_2));
    }

    protected File getApp1() {
        return new File(System.getProperty(APK_FILE_SYSTEM_PROPERTY_1));
    }

    @After
    public void tear_down() {
        if(takeScreenshotsOnDevice1Process != null) {
            takeScreenshotsOnDevice1Process.destroyForcibly();
        }
        if(takeScreenshotsOnDevice2Process != null) {
            takeScreenshotsOnDevice2Process.destroyForcibly();
        }
        if (device1 != null) {
            device1.quit();
        }
        if (device2 != null) {
            device2.quit();
        }
    }

    public void takeScreenshot(AndroidDriver device, String screenshotName) {
        File screenshot = device.getScreenshotAs(OutputType.FILE);
        String deviceName = device.getCapabilities().getCapability("deviceName").toString();
        File destFile = new File(BUILD_DIR + File.separator
                + deviceName + "-" + screenshotName + ".png");
        try {
            FileUtils.copyFile(screenshot, destFile);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public void takeScreenshotWithADB(AndroidDriver device, String screenshotName) throws Exception {
        String deviceName = device.getCapabilities().getCapability("deviceName").toString();
        String[] cmd1 = { "adb", "-s", deviceName, "shell", "\"mkdir /sdcard/test-screenshots\""};
        String[] cmd2 = { "adb", "-s", deviceName, "shell", "\"/system/bin/screencap -p /data/local/tmp/test-screenshots/" + screenshotName + ".png \""};
//        System.out.println(Arrays.toString(cmd1));
        File destFile = new File(BUILD_DIR + File.separator
                + deviceName + "-" + screenshotName + ".png");
//        String[] cmd3 = { "adb", "-s", deviceName, "pull", "/sdcard/Download/" + screenshotName + ".png", destFile.getAbsolutePath()};
        Process p1 = Runtime.getRuntime().exec(cmd1);
        p1.waitFor();
        Process p2 = Runtime.getRuntime().exec(cmd2);
        p2.waitFor();
//        Process p3 = Runtime.getRuntime().exec(cmd3);
//        p3.waitFor();
    }

    /**
     * Waits until application is initialized after clicking on the PhoneX icon.
     *
     * In other words it waits till either welcome fragment or login fragment
     * are shown.
     *
     * @param device
     */
    protected void waitUntillAppIsInitialized(final AndroidDriver device) { 
        WaitUtils.waitUntilPredicateHolds("App was not initialized in time!",
                device, TIMEOUT_APP_INIT, new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return !ElementUtils.getButtons(device).isEmpty()
                        && (introFragment.isActive(device) || loginFragment.isActive(device));
                    }
                });
    }
    
    protected void waitUntillAppIsInitializedA44(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("App was not initialized in time!",
                device, TIMEOUT_APP_INIT, new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return introFragment.isActiveA44(device) || loginFragment.isActiveA44(device);
                    }
                });
    }

    protected void setReinstalled(AndroidDriver device) {
        this.reinstalled = introFragment.isActive(device);
    }
    
    protected void setReinstalledA44(AndroidDriver device) {
        this.reinstalled = introFragment.isActiveA44(device);
    }

    /**
     * Determines whether PhoneX was reinstalled or not.
     *
     * In other words it finds out whether after launching the PhoneX welcome
     * fragment is shown, or the login one.
     *
     * @param device
     * @return
     */
    protected boolean wasReinstalled(AndroidDriver device) {
        if (reinstalled == null) {
            throw new IllegalStateException("You should waitUntillAppIsInitialized to initialise reinstalled property!");
        }
        return reinstalled;
    }

    /**
     * Logs in into the app.
     *
     * @param device
     * @param login
     * @throws Exception
     */
    protected void loginIntoApp(AndroidDriver device, String login) throws Exception {
        loginIntoApp(device, login, QA_PASSWORD);
    }

    protected void loginIntoApp(AndroidDriver device, String login, String passwd) throws Exception {
        if (wasReinstalled(device)) {
            introFragment.continueToLogin(device);
        }
        loginFragment.login(device, login, passwd);
    }
    
    protected void loginIntoAppA44(AndroidDriver device, String login, String passwd) throws Exception {
        if (wasReinstalled(device)) {
            introFragment.continueToLoginA44(device);
        }
        loginFragment.loginA44(device, login, passwd);
    }

    protected void waitUntilUserIsLoggedIn(final AndroidDriver device) {
        contactListFragment.waitUntilVisible(device, TIMEOUT_LOGIN);
    }
    
    protected void waitUntilUserIsLoggedInA44(final AndroidDriver device) {
        contactListFragment.waitUntilVisible(device, TIMEOUT_LOGIN);
    }
    
    protected void syncFileTransfersKeys(AndroidDriver device) throws Exception {
        hamburgerMenuFragment.show(device);
        hamburgerMenuFragment.tapMyAccount(device);
        myAccountFragment.waitUntilVisible(device);
        myAccountFragment.tapSyncKeys(device);
        device.navigate().back();
    }
    
    protected void sendFileToRemoteDevice(String deviceName, String filePath) throws Exception {
        String[] cmd = { "sh/sendFileToDevice.sh", getDeviceProperty(deviceName, "udid"), filePath};
        Process p = Runtime.getRuntime().exec(cmd);
    }
    
    protected void sendFileToRemoteDevice(String deviceName) throws Exception {
        File fileForFileTransferTest = new File("res/01-a-photoForFileTransfer.jpg");
        sendFileToRemoteDevice(deviceName, fileForFileTransferTest.getAbsolutePath());
    }
    
    protected Process startTakingOfScreenshots(String deviceName) throws Exception {
        String[] cmd = { "sh/take-screenshot-on-device.sh", getDeviceProperty(deviceName, "udid")};
        return Runtime.getRuntime().exec(cmd);
    }
}