package net.phonex.tests.fragments.call;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import java.util.List;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class SASDialogFragment {

    private static final String DIALOG_HEADER = "Security code";
    
    private static final String SAS_RID = "sasContainer";
    private static final String CONFIRM_SAS_BUTTON_RID = "buttonDefaultPositive";

    private static final int TIMEOUT_TO_SHOW = 20000;

    @Deprecated
    public void confirm(AndroidDriver device) throws Exception {
        waitUntilPresent(device);
        tapConfirm(device);
    }
    
    public void confirmA44(AndroidDriver device) throws Exception {
        waitUntilPresentA44(device);
        tapConfirmA44(device);
    }

    public void confirmIfPresent(AndroidDriver device) {
        try {
            waitUntilPresent(device);
            confirm(device);
        } catch (Exception ex) {
            //not present
        }
    }

    @Deprecated
    public void waitUntilPresent(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("SAS verification was not shown in time, on device: "
                + device.getCapabilities().getCapability("deviceName"),
                device, TIMEOUT_TO_SHOW, new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return isPresent(device);
                    }
                }
        );
    }
    
    public void waitUntilPresentA44(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("SAS verification was not shown in time, on device: "
                + device.getCapabilities().getCapability("deviceName"),
                device, TIMEOUT_TO_SHOW, new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return isPresentA44(device);
                    }
                }
        );
    }

    @Deprecated
    public boolean isPresent(AndroidDriver device) {
        List<WebElement> textViews = ElementUtils.getTextViews(device);
        return !textViews.isEmpty()
                && textViews.get(0)
                .getText().equals(DIALOG_HEADER);
    }
    
    public boolean isPresentA44(AndroidDriver device) {
        return ElementUtils.findElementByResourceId(device, SAS_RID) != null;
    }

    @Deprecated
    private void tapConfirm(AndroidDriver device) throws Exception {
        ElementUtils.getLastTextView(device).click();
    }
    
    private void tapConfirmA44(AndroidDriver device) throws Exception {
        ElementUtils.findElementByResourceId(device, CONFIRM_SAS_BUTTON_RID).click();
    }
}