package net.phonex.tests.fragments.contactlist;

import com.google.common.base.Predicate;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import java.util.Arrays;
import java.util.List;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class ContactListFragment {

    private static final int TIMEOUT_ADD_CONTACT_DIALOG_SHOWN = 4000;
    private static final int TIMEOUT_ADD_CONTACT_SERVER_COMMUNICATION = 20000;
    private static final int TIMEOUT_DELETE_CONTACT_SERVER_COMMUNICATION = 20000;
    private static final int TIMEOUT_CALL_CHAT_EDIT_SHOWN = 4000;
    private static final int TIMEOUT_CONTACT_LIST_VISIBLE = 20000;

    private static final String ADD_CONTACT_DIALOG_HEADER = "Add contact";
    private static final String CALL_CHAT_EDIT = "Call";
    private static final String CONTACT_FRAGMENT_HEADER = "Contacts";

    private static final int CALL_OPTION_INDEX = 1;
    private static final int SEND_MSG_OPTION_INDEX = 2;
    private static final int EDIT_OPTION_INDEX = 3;
    private static final int LONG_PRESS_DURATION = 2000;
    
    private static final String CONTACT_CONTAINER_RID = "contact_container";
    private static final String CALL_CHAT_EDIT_TITLE_RID = "title";
    private static final String CONTACT_NAME_RID = "name";

    public void longTapOverContact(AndroidDriver device, String contactName) {
        new TouchAction(device).longPress(getContactElement(device, contactName), LONG_PRESS_DURATION)
                .release().perform();
    }

    public void selectContacts(AndroidDriver device, String... contactNames) {
        List<WebElement> contacts = getContactElements(device);
        for (WebElement contact : contacts) {
            if (Arrays.asList(contactNames).contains(getContactNameFromElement(contact))) {
                contact.click();
            }
        }
    }

    public void tapBroadcastMessageIcon(AndroidDriver device) {
        ElementUtils.getLastTextView(device).click();
    }

    private WebElement getContactElement(AndroidDriver device, String contactName) {
        return getContactElements(device).get(getIndexOfContact(device, contactName));
    }

    public void dialContact(AndroidDriver device, int contactIndex)
            throws Exception {
        tapContactElement(device, contactIndex);
        getCallMenuOption(device).click();
    }
    
    public void dialContactA44(AndroidDriver device, int contactIndex)
            throws Exception {
        tapContactElementA44(device, contactIndex);
        getCallMenuOption(device).click();
    }

    public void dialContact(AndroidDriver device, String contactName) throws Exception {
        dialContact(device, getIndexOfContact(device, contactName));
    }
    
    public void dialContactA44(AndroidDriver device, String contactName) throws Exception {
        dialContactA44(device, getIndexOfContactA44(device, contactName));
    }

    @Deprecated
    public void gotoSendMessage(AndroidDriver device, String contactName) {
        tapContactElement(device, getIndexOfContact(device, contactName));
        getSendMessageMenuOption(device).click();
    }
    
    public void gotoSendMessageA44(AndroidDriver device, String contactName) {
        tapContactElementA44(device, getIndexOfContactA44(device, contactName));
        getSendMessageMenuOption(device).click();
    }

    public void tapNewMessageImage(AndroidDriver device, String contactName) {
        getContactElement(device, contactName)
                .findElement(By.className(ElementUtils.IMAGE_BUTTON))
                .click();
    }

    public void waitUntilNewMessageReceived(final AndroidDriver device, final String contactName, int timeout) {
        WaitUtils.waitUntilPredicateHolds("New message from contact " + contactName + " was not received in time!",
                device, timeout, new Predicate<WebDriver>() {

                    public boolean apply(WebDriver t) {
                        List<WebElement> contactListEl = getContactElements(device);
                        int index = getIndexOfContact(device, contactName);
                        return !contactListEl.isEmpty() && index != -1 && contactListEl.size() > index
                        && !contactListEl.get(index).findElements(By.className(ElementUtils.IMAGE_BUTTON)).isEmpty();
                    }
                });
    }

    @Deprecated
    private void tapContactElement(AndroidDriver device, int contactIndex) {
        getContactElements(device).get(contactIndex).click();
        waitUntilCallChatEditDialogIsShown(device, TIMEOUT_CALL_CHAT_EDIT_SHOWN);
    }
    
    private void tapContactElementA44(AndroidDriver device, int contactIndex) {
        getContactElementsA44(device).get(contactIndex).click();
        waitUntilCallChatEditDialogIsShownA44(device, TIMEOUT_CALL_CHAT_EDIT_SHOWN);
    }

    private WebElement getEditMenuOption(AndroidDriver device) {
        return ElementUtils.getTextViews(device).get(EDIT_OPTION_INDEX);
    }

    private WebElement getSendMessageMenuOption(AndroidDriver device) {
        return ElementUtils.getTextViews(device).get(SEND_MSG_OPTION_INDEX);
    }

    private WebElement getCallMenuOption(AndroidDriver device) {
        return ElementUtils.getTextViews(device).get(CALL_OPTION_INDEX);
    }

    @Deprecated
    private int getIndexOfContact(AndroidDriver device, String contactName) {
        int indexOfContact = 0;
        boolean wasFound = false;
        for (WebElement element : getContactElements(device)) {
            if (getContactNameFromElement(element).equals(contactName)) {
                wasFound = true;
                break;
            }
            indexOfContact++;
        }
        return wasFound ? indexOfContact : -1;
    }

    private int getIndexOfContactA44(AndroidDriver device, String contactName) {
        int indexOfContact = 0;
        boolean wasFound = false;
        for (WebElement element : getContactElementsA44(device)) {
            if (getContactNameFromElementA44(element).equals(contactName)) {
                wasFound = true;
                break;
            }
            indexOfContact++;
        }
        return wasFound ? indexOfContact : -1;
    }
    
    @Deprecated
    private String getContactNameFromElement(WebElement contactElement) {
        return contactElement.findElements(By.className(ElementUtils.TEXT_VIEW)).get(0).getText();
    }
    
    private String getContactNameFromElementA44(WebElement contactElement) {
        return contactElement.findElement(By.id(CONTACT_NAME_RID)).getText();
    }

    @Deprecated
    private void waitUntilCallChatEditDialogIsShown(final AndroidDriver device, int timeout) {
        WaitUtils.waitUntilPredicateHolds("Call, Chat, Edit dialog was not shown in time!",
                device, timeout, new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return !ElementUtils.getTextViews(device).isEmpty()
                        && ElementUtils.getTextViews(device).get(1).getText().equals(CALL_CHAT_EDIT);
                    }
                }
        );
    }
    
    private void waitUntilCallChatEditDialogIsShownA44(final AndroidDriver device, int timeout) {
        WaitUtils.waitUntilPredicateHolds("Call, Chat, Edit dialog was not shown in time!",
                device, timeout, new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return ElementUtils.findElementByResourceId(device, CALL_CHAT_EDIT_TITLE_RID) != null;
                    }
                }
        );
    }

    @Deprecated
    private List<WebElement> getContactElements(AndroidDriver device) {
        return ElementUtils.getRecyclerViews(device).get(0)
                .findElements(By.className(ElementUtils.LINEAR_LAYOUT));
    }
    
    private List<WebElement> getContactElementsA44(AndroidDriver device) {
        return ElementUtils.findElementsByResourceId(device, CONTACT_CONTAINER_RID);
    }

    public boolean isThereBigAddContactIcon(AndroidDriver device) {
        return ElementUtils.getTextViews(device).get(1).isDisplayed();
    }

    public void addContact(final String username, final AndroidDriver device) {
        //icon for adding contact is last TextView
        ElementUtils.getLastTextView(device).click();
        //wait till add contact dialog is shown
        WaitUtils.waitUntilPredicateHolds("Dialog for contact adding was not shown in time!", device, TIMEOUT_ADD_CONTACT_DIALOG_SHOWN,
                new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        List<WebElement> textViews = ElementUtils.getTextViews(device);
                        return !textViews.isEmpty()
                        && textViews.get(0).getText().equals(ADD_CONTACT_DIALOG_HEADER);
                    }
                });
        //write new contact to input
        ElementUtils.getEditTexts(device).get(0).clear();
        ElementUtils.getEditTexts(device).get(0).sendKeys(username);
        //click add button
        ElementUtils.getLastTextView(device).click();
        //wait till added
        WaitUtils.waitUntilPredicateHolds("Contact was not added in time!", device,
                TIMEOUT_ADD_CONTACT_SERVER_COMMUNICATION, new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        for (WebElement element : ElementUtils.getTextViews(device)) {
                            if (element.getText().equals(username)) {
                                return true;
                            }
                        }
                        return false;
                    }
                });
    }
    
    public void removeContact(final String contactName, final AndroidDriver device) {
        tapContactElement(device, getIndexOfContact(device, contactName));
        waitUntilCallChatEditDialogIsShown(device, TIMEOUT_CALL_CHAT_EDIT_SHOWN);
        getEditMenuOption(device).click();
        getDeleteAccountOption(device).click();
        //click OK button
        ElementUtils.getLastTextView(device).click();
        waitUntilContactIsRemoved(contactName, device);
    }
    
    private void waitUntilContactIsRemoved(final String contactName, final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Contact was not removed in time!", device, 
                TIMEOUT_DELETE_CONTACT_SERVER_COMMUNICATION, new Predicate<WebDriver>() {

            public boolean apply(WebDriver t) {
                return !ElementUtils.getRecyclerViews(device).isEmpty() &&
                        !ElementUtils.getLinearLayouts(device).isEmpty() &&
                        getIndexOfContact(device, contactName) == -1;
            }
        });
    }
    
    private WebElement getDeleteAccountOption(AndroidDriver device) {
        return ElementUtils.getTextViews(device).get(3);
    }

    public void waitUntilVisible(final AndroidDriver device, int timeout) {
        WaitUtils.waitUntilPredicateHolds("Contact list was not shown in time!", device, timeout,
                new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return !ElementUtils.getTextViews(device).isEmpty()
                        && ElementUtils.getTextViews(device).get(0).getText().equals(CONTACT_FRAGMENT_HEADER)
                        && ElementUtils.getProgressBars(device).isEmpty();
                    }
                });
    }
    
    public void waitUntilContactsAreVisibleA44(final AndroidDriver device, int timeout) {
        WaitUtils.waitUntilPredicateHolds("Contact list was not shown in time!", device, timeout,
                new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return ElementUtils.findElementByResourceId(device, CONTACT_CONTAINER_RID) != null
                        && ElementUtils.getProgressBars(device).isEmpty();
                    }
                });
    }

    public void waitUntilVisible(final AndroidDriver device) {
        waitUntilVisible(device, TIMEOUT_CONTACT_LIST_VISIBLE);
    }
}