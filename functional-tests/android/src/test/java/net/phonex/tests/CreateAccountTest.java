package net.phonex.tests;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import net.phonex.tests.utils.AsyncUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jhuska
 */
public class CreateAccountTest extends AbstractTest {

    private static final String QA_CAPTCHA = "captcha";
    private static final String QA_USER_NAME = "foobar";
    private static final int CALL_LENGTH = 6000;
    private static final int TIMEOUT_CALL_HUNG_UP = 6000;

    private Future<String> secondDeviceFT = null;

    @Before
    public void set_up() throws Exception {
        secondDeviceFT = AsyncUtils.executeAsyncTask(new Callable<String>() {
            public String call() throws Exception {
                set_up_device2();
                loginIntoApp(device2, QA_LOGIN_DEVICE_2);
                waitUntilUserIsLoggedIn(device2);
                return "done";
            }
        });
        set_up_device1();
    }

    @Test
    public void testObtainLicence() throws Exception {
        if (wasReinstalled(device1)) {
            introFragment.createAccount(device1);
        } else {
            loginFragment.createNewAccount(device1);
        }
        accountCreationFragment.waitTillActive(device1);
        accountCreationFragment.typeUserName(QA_USER_NAME, device1);
        accountCreationFragment.typeCaptcha(QA_CAPTCHA, device1);
        accountCreationFragment.submit(device1);
        final String newLogin = successDialogFragment.logIn(device1);
        changeDefaultPasswdFragment.typeNewPassword(QA_PASSWORD, device1);
        changeDefaultPasswdFragment.typePasswdConfirmation(QA_PASSWORD, device1);
        changeDefaultPasswdFragment.save(device1);
        waitUntilUserIsLoggedIn(device1);
        Assert.assertTrue("Trial licence was not obtained! User was not able to login!",
                contactListFragment.isThereBigAddContactIcon(device1));

        //now try to add created licence on another device and vice versa
        secondDeviceFT.get();
        contactListFragment.addContact(newLogin, device2);
        contactListFragment.addContact(QA_LOGIN_DEVICE_2, device1);

        //call from trial account to already created
        contactListFragment.waitUntilVisible(device1);
        contactListFragment.dialContact(device1, QA_LOGIN_DEVICE_2);
        Thread.sleep(CALL_LENGTH);
        callFragment.answerCall(device2, DEVICE_2_NAME);
        sasDialogFragment.confirm(device1);
        sasDialogFragment.confirm(device2);
        callFragment.hungUp(device1);
        contactListFragment.waitUntilVisible(device1, TIMEOUT_CALL_HUNG_UP);
        contactListFragment.removeContact(newLogin, device2);
    }
}