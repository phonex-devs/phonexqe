package net.phonex.tests;

import java.math.BigInteger;
import java.security.SecureRandom;
import net.phonex.tests.fragments.chat.ChatFragment.Message;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author jhuska
 */
public class MessagingTest extends AbstractTestTwoDevices {

    public static final int TIMEOUT_MESSAGE_RECEIVED = 10000;

    @Test
    public void test_send_message() throws Exception {
        contactListFragment.gotoSendMessage(device1, QA_LOGIN_DEVICE_2);
        chatFragment.waitUntilVisible(device1);
        String randomString = getRandomString();
        Message toSend = new Message();
        toSend.setText(randomString);
        chatFragment.sendMessage(device1, toSend);

        contactListFragment.waitUntilNewMessageReceived(device2, QA_LOGIN_DEVICE_1, TIMEOUT_MESSAGE_RECEIVED);
        contactListFragment.tapNewMessageImage(device2, QA_LOGIN_DEVICE_1);
        chatFragment.waitUntilVisible(device2);
        Assert.assertTrue("The sent message was not received correctly!",
                chatFragment.getMessages(device2).contains(toSend));

        chatFragment.waitUntilSeen(device1);
    }

    @Test
    public void test_forward_message() throws Exception {
        test_send_message();
        chatFragment.forwardMessage(chatFragment.getLastMessageBlockElement(device1), device1);
        forwardMessageFragment.waitUntilVisible(device1);
        forwardMessageFragment.fillInRecipients(device1, QA_LOGIN_DEVICE_2);
        String forwardedMsgAddition = "forwarded:";
        forwardMessageFragment.fillInMessageBox(forwardedMsgAddition, device1);
        forwardMessageFragment.tapSendButton(device1);
        chatFragment.waitUntilMessageWithContentReceived(forwardedMsgAddition, device2);
    }

    public static String getRandomString() {
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }
}
