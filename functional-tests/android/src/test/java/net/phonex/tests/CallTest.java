package net.phonex.tests;

import org.junit.Test;
import org.junit.Assert;

public class CallTest extends AbstractTestTwoDevices {

    private static final int NUMBER_OF_CALLS = 10;

    private static final int CALL_LENGTH = 6000;
    private static final int DIALING_LENGTH = 4000;
    
    private static final int TIMEOUT_CALL_HUNG_UP = 10000;

    @Test
    public void test_caller_hungs_up_before_answering() throws Exception {
        for (int i = 0; i < NUMBER_OF_CALLS; i++) {
            printInfo(i);
            callAndHungUpBeforeAnswering();
        }
    }

    @Test
    public void test_callee_answer_call() throws Exception {
        for (int i = 0; i < NUMBER_OF_CALLS; i++) {
            printInfo(i);
            callAndAnswer(i);
        }
    }
    
    private void printInfo(int i) {
        System.out.println("Call attempt #" + (i+1));
    }

    private void callAndAnswer(int i) throws Exception {
        contactListFragment.dialContact(device1, QA_LOGIN_DEVICE_2);
        callFragment.waitUntillCallIsIncoming(device2);
        Thread.sleep(DIALING_LENGTH);

        callFragment.answerCall(device2, DEVICE_2_NAME);

        if (i == 0) {
            try {
                sasDialogFragment.confirm(device1);
                sasDialogFragment.confirm(device2);
            } catch (Exception ex) {
                //OK there was no SAS
            }

        }
        Thread.sleep(CALL_LENGTH);

        callFragment.hungUp(device1);
        contactListFragment.waitUntilVisible(device1, TIMEOUT_CALL_HUNG_UP);
    }

    private void callAndHungUpBeforeAnswering() throws Exception {
        contactListFragment.dialContact(device1, QA_LOGIN_DEVICE_2);
        Thread.sleep(DIALING_LENGTH);
        callFragment.hungUp(device1);
        contactListFragment.waitUntilVisible(device2, TIMEOUT_CALL_HUNG_UP);

        appTabsMenuFragment.goToNotificationTab(device2);
        notificationFragment.waitUntilVisible(device2);
        Assert.assertEquals("Missed call not logged!", QA_LOGIN_DEVICE_1, 
                notificationFragment.getMissedCalls(device2, QA_LOGIN_DEVICE_1).get(0));
        notificationFragment.deleteAllNotifications(device2);
        appTabsMenuFragment.goToContactsTab(device2);
        contactListFragment.waitUntilVisible(device2, TIMEOUT_TAB_SWITCH);
    }
}