package net.phonex.tests.fragments.auth;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author jhuska
 */
public class LoginFragment {

    private static final String SIGNIN_BUTTON_TEXT = "Sign in";

    private static final int TIMEOUT_LOGIN_SHOWN = 2000;
    
    private static final String LOGIN_INPUT_RID = "login";
    private static final String PASSWORD_INPUT_RID = "password";
    private static final String SIGN_IN_BUTTON_RID = "setup_submit";

    public void login(AndroidDriver device, String login, String password)
            throws Exception {
        waitUntillActive(device);
        ElementUtils.getEditTexts(device).get(0).sendKeys(login);
        ElementUtils.getEditTexts(device).get(1).sendKeys(password);
        ElementUtils.getButtons(device).get(0).click();
    }
    
    public void loginA44(AndroidDriver device, String login, String password)
            throws Exception {
        waitUntillActiveA44(device);
        ElementUtils.findElementByResourceId(device, LOGIN_INPUT_RID).sendKeys(login);
        ElementUtils.findElementByResourceId(device, PASSWORD_INPUT_RID).sendKeys(password);
        ElementUtils.findElementByResourceId(device, SIGN_IN_BUTTON_RID).click();
    }
    
    public void createNewAccount(AndroidDriver device) {
        ElementUtils.getTextViews(device).get(1).click();
    }

    public boolean isActive(AndroidDriver device) {
        return !ElementUtils.getButtons(device).isEmpty()
                && ElementUtils.getButtons(device).get(0).getText().equals(SIGNIN_BUTTON_TEXT);
    }
    
    public boolean isActiveA44(AndroidDriver device) {
        return ElementUtils.findElementByResourceId(device, LOGIN_INPUT_RID) != null
                &&
                ElementUtils.findElementByResourceId(device, PASSWORD_INPUT_RID) != null;
    }

    public void waitUntillActive(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Login screen was not shown in time!", 
                device, TIMEOUT_LOGIN_SHOWN, new Predicate<WebDriver>() {
            public boolean apply(WebDriver t) {
                return isActive(device);
            }
        });
    }
    
    public void waitUntillActiveA44(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Login screen was not shown in time!", 
                device, TIMEOUT_LOGIN_SHOWN, new Predicate<WebDriver>() {
            public boolean apply(WebDriver t) {
                return isActiveA44(device);
            }
        });
    }
}