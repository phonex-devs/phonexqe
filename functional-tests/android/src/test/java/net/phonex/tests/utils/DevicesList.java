package net.phonex.tests.utils;

import java.io.FileReader;
import static net.phonex.tests.AbstractTest.DEVICE_LIST;
import static net.phonex.tests.AbstractTest.SWIPE_TO_ANSWER_CALL_PROPERTY;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author jhuska
 */
public class DevicesList {

    public static JSONObject getDevicesListJSON() throws Exception {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(DEVICE_LIST));
        return (JSONObject) obj;
    }

    public static String getDeviceProperty(String device, String property) throws Exception {
        JSONObject deviceRoot = (JSONObject) getDevicesListJSON().get(device);
        return (String) deviceRoot.get(property);
    }

    public static JSONObject getSwipeToAnswerCallProperty(String deviceName) throws Exception {
        JSONObject deviceRoot = (JSONObject) getDevicesListJSON().get(deviceName);
        return (JSONObject) deviceRoot.get(SWIPE_TO_ANSWER_CALL_PROPERTY);
    }
}
