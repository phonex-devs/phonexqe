package net.phonex.tests.fragments.auth;

import io.appium.java_client.android.AndroidDriver;
import java.util.List;
import net.phonex.tests.utils.ElementUtils;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class IntroFragment {
    
    private static final String CREATE_ACCOUNT_BUTTON_TXT = "Create account";
    
    private static final String CREATE_ACCOUNT_RID = "button_create_trial_license";
    private static final String SIGN_IN_RID = "button_sign_in";
    
    public void continueToLogin(AndroidDriver device) throws Exception {
        ElementUtils.getButtons(device).get(1).click();
    }
    
    public void continueToLoginA44(AndroidDriver device) throws Exception {
        ElementUtils.findElementByResourceId(device, SIGN_IN_RID).click();
    }
    
    public void createAccount(AndroidDriver device) throws Exception {
        ElementUtils.getButtons(device).get(0).click();
    }
    
    public boolean isActive(AndroidDriver device) {
        List<WebElement> buttons = ElementUtils.getButtons(device);
        return buttons.size() > 1 && buttons.get(0).getText().equals(CREATE_ACCOUNT_BUTTON_TXT);
    }
    
    public boolean isActiveA44(AndroidDriver device) {
        return ElementUtils.findElementByResourceId(device, SIGN_IN_RID) != null
                && ElementUtils.findElementByResourceId(device, CREATE_ACCOUNT_RID) != null;
    }
}