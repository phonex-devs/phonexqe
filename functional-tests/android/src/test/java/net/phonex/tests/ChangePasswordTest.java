package net.phonex.tests;

import net.phonex.tests.utils.ElementUtils;
import org.junit.Test;
import org.openqa.selenium.Capabilities;

/**
 *
 * @author jhuska
 */
public class ChangePasswordTest extends AbstractTestOneDevice {
    
    private static final String NEW_PASSWD = "xxxxxxxx1";
    
    private static final int TIMEOUT_PASSWD_CHANGED = 20000;
    
    @Test
    public void test() throws Exception {
        tryToChangePassword(NEW_PASSWD);
        contactListFragment.waitUntilVisible(device1);
        hamburgerMenuFragment.show(device1);
        hamburgerMenuFragment.tapLogOut(device1);
        Capabilities capabilities = device1.getCapabilities();
        device1.startActivity((String)capabilities.getCapability("appPackage"), 
                (String) capabilities.getCapability("appActivity"));
        waitUntillAppIsInitialized(device1);
        setReinstalled(device1);
        loginIntoApp(device1, QA_LOGIN_DEVICE_1, NEW_PASSWD);
        waitUntilUserIsLoggedIn(device1);
        tryToChangePassword(QA_PASSWORD);
        contactListFragment.waitUntilVisible(device1);
    }
    
    private void tryToChangePassword(String newPasswd) throws Exception {
        hamburgerMenuFragment.show(device1);
        hamburgerMenuFragment.tapMyAccount(device1);
        myAccountFragment.waitUntilVisible(device1);
        myAccountFragment.tapChangePassword(device1);
        newPasswordFragment.waitUntilVisible(device1);
        newPasswordFragment.set(newPasswd, QA_PASSWORD, device1);
        myAccountFragment.waitUntilVisible(device1, TIMEOUT_PASSWD_CHANGED);
        ElementUtils.getImageButtons(device1).get(0).click();
    }
}