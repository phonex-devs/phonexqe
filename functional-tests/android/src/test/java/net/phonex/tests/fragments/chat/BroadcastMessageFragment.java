package net.phonex.tests.fragments.chat;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import java.util.List;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class BroadcastMessageFragment {

    private static final int TIMEOUT_SHOWN = 4000;
    
    private static final String HEADER_TXT = "Broadcast message";
    
    public void typeMessage(AndroidDriver device, String message) {
        ElementUtils.getEditTexts(device).get(0).sendKeys(message);
    }
    
    public void tapSendButton(AndroidDriver device) {
        ElementUtils.getTextViews(device).get(2).click();
    }
    
    public void tapAttachmentIcon(AndroidDriver device) {
        ElementUtils.getTextViews(device).get(1).click();
    }
    
    public void waitUntilVisible(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Broadcast message screen was not shown in time!", 
                device, TIMEOUT_SHOWN, new Predicate<WebDriver>() {

            public boolean apply(WebDriver t) {
                List<WebElement> textViews = ElementUtils.getTextViews(device);
                return !textViews.isEmpty() && textViews.get(0).getText().equals(HEADER_TXT);
            }
        });
    }
}