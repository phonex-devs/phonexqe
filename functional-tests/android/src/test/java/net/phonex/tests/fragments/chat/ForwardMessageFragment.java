package net.phonex.tests.fragments.chat;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author jhuska
 */
public class ForwardMessageFragment {

    public static final String HEADER = "Forward message";

    public static final int TIMEOUT_SHOWN = 4000;

    public void fillInRecipients(AndroidDriver device, String recipient) {
        ElementUtils.getMultiAutoCompleteTextViews(device).get(0).sendKeys(recipient);
        device.sendKeyEvent(66); //Enter key
    }

    public void fillInMessageBox(String text, AndroidDriver device) {
        ElementUtils.getEditTexts(device).get(0).sendKeys(text);
    }

    public void tapSendButton(AndroidDriver device) {
        ElementUtils.getTextViews(device).get(1).click();
    }

    public void waitUntilVisible(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Forward message screen was not shown in time!",
                device, TIMEOUT_SHOWN, new Predicate<WebDriver>() {

                    public boolean apply(WebDriver t) {
                        return ElementUtils.getTextView(device, HEADER) != null;
                    }
                });
    }
}
