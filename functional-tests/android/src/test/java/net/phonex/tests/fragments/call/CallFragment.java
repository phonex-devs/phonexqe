package net.phonex.tests.fragments.call;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import java.util.List;
import net.phonex.tests.utils.DevicesList;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.json.simple.JSONObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class CallFragment {

    private static final String INCOMING_CALL_TEXT = "Incoming call";
    
    private static final String END_BUTTON_RID = "endButton";
    
    private static final int TIMEOUT_FOR_CALL_IS_INCOMING = 15000;
    private static final int TIMOUT_CALL_IS_ANSWERED = 15000;
    private static final int SWIPE_LENGTH = 2;
    
    public static final int TIMOUT_CALL_IS_HANG_UP = 10000;

    @Deprecated
    public void hungUp(AndroidDriver device) throws Exception {
        ElementUtils.getImageButtons(device).get(0).click();
    }
    
    public void hungUpA44(AndroidDriver device) throws Exception {
        ElementUtils.findElementByResourceId(device, END_BUTTON_RID).click();
    }
    
    public void answerCall(AndroidDriver device, String deviceName) throws Exception {
        try {
            JSONObject swipeProperty = DevicesList.getSwipeToAnswerCallProperty(deviceName);
            int xStart = Integer.parseInt((String) swipeProperty.get("xs"));
            int xEnd = Integer.parseInt((String) swipeProperty.get("xe"));
            int y = Integer.parseInt((String) swipeProperty.get("y"));
            device.swipe(xStart, y, xEnd, y, SWIPE_LENGTH);
        } catch (WebDriverException ex) {
            //OK, because of bug in uiautomator in Android 4.2.2
        }
        waitUntillCallIsAnswered(device);
    }
    
    public void waitUntillCallIsAnswered(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Call was not answered in time!", device, 
                TIMOUT_CALL_IS_ANSWERED, new Predicate<WebDriver>() {
            public boolean apply(WebDriver t) {
                return !isCallIncoming(device);
            }
        });
    }

    public void waitUntillCallIsIncoming(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("No incoming call!", 
                device, TIMEOUT_FOR_CALL_IS_INCOMING, new Predicate<WebDriver>() {
            public boolean apply(WebDriver t) {
                return isCallIncoming(device);
            }
        });
    }

    public boolean isCallIncoming(AndroidDriver device) {
        List<WebElement> textViews = ElementUtils.getTextViews(device);
        return textViews.size() > 1
                && textViews.get(1).getText().equals(INCOMING_CALL_TEXT);
    }
}
