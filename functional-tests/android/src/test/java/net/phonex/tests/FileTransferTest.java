package net.phonex.tests;

import org.junit.Test;

/**
 *
 * @author jhuska
 */
public class FileTransferTest extends AbstractTestTwoDevices {

    @Test
    public void test_file_transfer() throws Exception {
        sendFileToRemoteDevice(DEVICE_1_NAME);

        syncFileTransfersKeys(device1);
        syncFileTransfersKeys(device2);
        Thread.sleep(TIMEOUT_FILE_TRANSFER_KEYS_SYNCED);

        contactListFragment.gotoSendMessage(device1, QA_LOGIN_DEVICE_2);
        chatFragment.waitUntilVisible(device1);
        chatFragment.tapAttachmentButton(device1);

        filePickerFragment.waitUntilVisible(device1);
        filePickerFragment.gotoDownload(device1);
        filePickerFragment.selectFile(FILE_TO_SEND_1_NAME, device1);
        filePickerFragment.tapSendButton(device1);

        contactListFragment.waitUntilNewMessageReceived(device2, QA_LOGIN_DEVICE_1, TIMEOUT_FILE_SENT);
        contactListFragment.tapNewMessageImage(device2, QA_LOGIN_DEVICE_1);
        chatFragment.waitUntilVisible(device2);
        chatFragment.waitUntilFileIsDownloaded(device2, FILE_TO_SEND_1_NAME);
    }

    @Test
    public void test_forward_file_message() throws Exception {
        test_file_transfer();
        chatFragment.goBackToContactList(device2);
        chatFragment.forwardMessage(chatFragment.getLastMessageBlockElement(device1), device1);
        forwardMessageFragment.waitUntilVisible(device1);
        forwardMessageFragment.fillInRecipients(device1, QA_LOGIN_DEVICE_2);
        forwardMessageFragment.tapSendButton(device1);
        contactListFragment.waitUntilNewMessageReceived(device2, QA_LOGIN_DEVICE_1,
                TIMEOUT_FILE_SENT);
    }
}
