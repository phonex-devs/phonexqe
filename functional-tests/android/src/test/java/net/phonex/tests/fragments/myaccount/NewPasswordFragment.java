package net.phonex.tests.fragments.myaccount;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import java.util.List;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class NewPasswordFragment {
    
    private static final int TIMEOUT_SHOWN = 3000;
    
    private static final String SCREEN_HEADER = "Change password";

    public void set(String newPasswd, String oldPassword, AndroidDriver device) {
        WebElement oldPasswdEditText =  ElementUtils.getEditTexts(device).get(0);
        oldPasswdEditText.clear();
        oldPasswdEditText.sendKeys(oldPassword);
        WebElement newPasswdEditText = ElementUtils.getEditTexts(device).get(1);
        newPasswdEditText.clear();
        newPasswdEditText.sendKeys(newPasswd);
        WebElement confirmationPasswdEditText = ElementUtils.getEditTexts(device).get(2);
        confirmationPasswdEditText.clear();
        confirmationPasswdEditText.sendKeys(newPasswd);
        ElementUtils.getButtons(device).get(0).click();
    }
    
    public void waitUntilVisible(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Set new password screen was not shown in time!", 
                device, TIMEOUT_SHOWN, new Predicate<WebDriver>() {

            public boolean apply(WebDriver t) {
                List<WebElement> textViews = ElementUtils.getTextViews(device);
                return !textViews.isEmpty() &&
                        textViews.get(0).getText().equals(SCREEN_HEADER);
            }
        });
    }
}
