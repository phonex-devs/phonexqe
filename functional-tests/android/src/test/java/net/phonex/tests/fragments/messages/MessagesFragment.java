package net.phonex.tests.fragments.messages;

import com.google.common.base.Predicate;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import java.util.List;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class MessagesFragment {

    private static final String MESSAGE_ICON_RID = "message_icon";

    private static final int TIMEOUT_POPUP_VISIBLE = 3000;
    private static final int TIMEOUT_THREAD_DELETED = 3000;

    public void removeConversationThread(AndroidDriver device, int indexOfThread) {
        List<WebElement> threads = ElementUtils.findElementsByResourceId(device, MESSAGE_ICON_RID);
        TouchAction action = new TouchAction(device);
        action.longPress(threads.get(indexOfThread)).waitAction(TIMEOUT_POPUP_VISIBLE).perform();
        WebElement deleteThread = ElementUtils.getTextViews(device).get(1);
        deleteThread.click();
        ElementUtils.getLastTextView(device).click();
        waitUntilThereAreNoThreads(device);
    }

    private void waitUntilThereAreNoThreads(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Messages threads were not deleted!", device, TIMEOUT_THREAD_DELETED,
                new Predicate<WebDriver>() {

                    @Override
                    public boolean apply(WebDriver t) {
                        return ElementUtils.findElementByResourceId(device, MESSAGE_ICON_RID) == null;
                    }
                });
    }
}
