package net.phonex.tests.utils;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 *
 * @author jhuska
 */
public class AsyncUtils {

    public static <T> FutureTask<T> executeAsyncTask(Callable<T> callable) {
        ExecutorService exService = Executors.newSingleThreadExecutor();
        FutureTask<T> ft = new FutureTask<T>(callable);
        exService.execute(ft);
        return ft;
    }
}