package net.phonex.tests.fragments.account;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author jhuska
 */
public class ChangeDefaultPasswordFragment {

    private static final int TIMEOUT_PROMT_FOR_CHANGING_PRESENT = 10000;
    
    private static final String CHANGE_DIALOG_HEADER = "Change default password";
    
    public void typeNewPassword(String passwd, final AndroidDriver device) throws Exception {
        waitUntilChageDefaultDialogPresent(device);
        ElementUtils.getEditTexts(device).get(0).sendKeys(passwd);
    }
    
    public void typePasswdConfirmation(String passwd, AndroidDriver device) throws Exception {
        ElementUtils.getEditTexts(device).get(1).sendKeys(passwd);
    }
    
    public void save(AndroidDriver device) {
        device.hideKeyboard();
        ElementUtils.getLastTextView(device).click();
    }
    
    private void waitUntilChageDefaultDialogPresent(final AndroidDriver driver) {
        WaitUtils.waitUntilPredicateHolds("Change default password screen was not shown in time!", 
                driver, TIMEOUT_PROMT_FOR_CHANGING_PRESENT, 
                 new Predicate<WebDriver>() {
             public boolean apply(WebDriver t) {
                 return !ElementUtils.getTextViews(driver).isEmpty()
                         &&
                         (ElementUtils.getTextViews(driver).get(0).getText()
                                 .equals(CHANGE_DIALOG_HEADER));
             }
         });
    }
}