package net.phonex.tests.fragments.notification;

import com.google.common.base.Predicate;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import java.util.ArrayList;
import java.util.List;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class NotificationsFragment {

    private static final String NOTIFICATIONS_FRAGMENT_HEADER = "Notifications";

    private static final int TIMEOUT_NOTIFICATION_VISIBLE_UI_OPERATION = 2000;
    private static final int TIMEOUT_POPUP_VISIBLE = 2000;
    private static final int TIMEOUT_NOTIFICATIONS_DELETED = 2000;

    public List<String> getMissedCalls(AndroidDriver device, String fromWhom) {
        List<String> result = new ArrayList<String>();
        for (WebElement missedCall : getMissedCallsElements(device)) {
            String actualMissedCall = missedCall.getText();
            if (actualMissedCall.equals(fromWhom)) {
                result.add(actualMissedCall);
            }
        }
        return result;
    }

    private List<WebElement> getMissedCallsElements(AndroidDriver device) {
        WebElement listView = ElementUtils.getListViews(device).get(0);
        List<WebElement> relativeLayouts = listView.findElements(By.className(ElementUtils.RELATIVE_LAYOUT));
        List<WebElement> result = new ArrayList<WebElement>();
        for (WebElement element : relativeLayouts) {
            result.add(element.findElement(By.className(ElementUtils.TEXT_VIEW)));
        }
        return result;
    }

    public void deleteAllNotifications(AndroidDriver device) {
        TouchAction action = new TouchAction(device);
        action.longPress(getMissedCallsElements(device).get(0)).waitAction(TIMEOUT_POPUP_VISIBLE).perform();
        WebElement deleteAllMissedCalls = ElementUtils.getTextViews(device).get(1);
        deleteAllMissedCalls.click();
        waitUntillThereAreNoNotifications(device);
    }

    private void waitUntillThereAreNoNotifications(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Notifications about missed calls did not disappear in time!",
                device, TIMEOUT_NOTIFICATIONS_DELETED, new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return isVisible(device)
                        && ElementUtils.getListViews(device).isEmpty();
                    }
                }
        );
    }

    public boolean isVisible(AndroidDriver device) {
        return !ElementUtils.getTextViews(device).isEmpty()
                && ElementUtils.getTextViews(device).get(0).getText().equals(NOTIFICATIONS_FRAGMENT_HEADER);
    }

    public void waitUntilVisible(final AndroidDriver device, int timeout) {
        WaitUtils.waitUntilPredicateHolds("Notification tab screen was not shown in time!",
                device, timeout,
                new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return isVisible(device);
                    }
                });
    }

    public void waitUntilVisible(final AndroidDriver device) {
        waitUntilVisible(device, TIMEOUT_NOTIFICATION_VISIBLE_UI_OPERATION);
    }
}