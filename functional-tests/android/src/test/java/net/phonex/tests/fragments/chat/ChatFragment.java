package net.phonex.tests.fragments.chat;

import com.google.common.base.Predicate;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import java.util.ArrayList;
import java.util.List;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class ChatFragment {

    private static final int TIMEOUT_SHOWN = 6000;
    private static final int TIMEOUT_SEEN = 10000;
    private static final int TIMEOUT_FILE_DOWNLOADED = 20000;
    
    public static final int TIMEOUT_MESSAGE_SENT = 15000;

    private static final String SEND_BUTTON_TEXT = "Send";
    private static final String SEEN_TEXT = "Seen";
    private static final String DOWNLOAD_BUTTON_TEXT = "Download";
    private static final String FILE_PREVIEW = "File preview";
    private static final String FORWARD_MESSAGE = "Forward message";
    
    private static final String SEND_BUTTON_RID = "send_button";
    private static final String MESSAGE_INPUT_RID = "embedded_text_editor";

    public void tapAttachmentButton(AndroidDriver device) {
        ElementUtils.getTextViews(device).get(1).click();
    }

    public void tapDownloadButton(AndroidDriver device) {
        ElementUtils.getButton(device, DOWNLOAD_BUTTON_TEXT).click();
    }

    public void tapFileReference(AndroidDriver device, String fileName) {
        ElementUtils.getTextView(device, fileName).click();
    }

    public void waitUntilFileIsDownloaded(final AndroidDriver device, final String fileToBeDownloaded) {
        WaitUtils.waitUntilPredicateHolds("Downloading of file was not finished in time!",
                device, TIMEOUT_FILE_DOWNLOADED, new Predicate<WebDriver>() {

                    public boolean apply(WebDriver t) {
                        return ElementUtils.getProgressBars(device).isEmpty()
                        && ElementUtils.getTextView(device, FILE_PREVIEW) == null
                        && ElementUtils.getTextView(device, fileToBeDownloaded) != null;
                    }
                });
    }

    @Deprecated
    public void sendMessage(AndroidDriver device, Message message) {
        WebElement input = ElementUtils.getEditTexts(device).get(0);
        input.clear();
        input.sendKeys(message.getText());
        ElementUtils.getButtons(device).get(0).click();
    }
    
    @Deprecated
    public void sendMessage(AndroidDriver device, String message) {
        WebElement input = ElementUtils.getEditTexts(device).get(0);
        input.clear();
        input.sendKeys(message);
        ElementUtils.getButtons(device).get(0).click();
    }
    
    public void sendMessageA44(AndroidDriver device, String message) {
        WebElement input = ElementUtils.findElementByResourceId(device, MESSAGE_INPUT_RID);
//        input.clear();
//        ((AndroidElement) input).
        input.sendKeys(message);
        getSendButton(device).click();
    }

    @Deprecated
    public void waitUntilVisible(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Messaging screen was not shown in time!",
                device, TIMEOUT_SHOWN, new Predicate<WebDriver>() {

                    public boolean apply(WebDriver t) {
                        return !ElementUtils.getButtons(device).isEmpty()
                        && ElementUtils.getButton(device, SEND_BUTTON_TEXT) != null;
                    }
                });
    }
    
    private WebElement getSendButton(AndroidDriver device) {
        return ElementUtils.findElementByResourceId(device, SEND_BUTTON_RID);
    }
    
    public void waitUntilVisibleA44(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Messaging screen was not shown in time!",
                device, TIMEOUT_SHOWN, new Predicate<WebDriver>() {

                    public boolean apply(WebDriver t) {
                        return getSendButton(device) != null;
                    }
                });
    }

    public void waitUntilMessageWithContentReceived(final String content, final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Message containing content: " + content
                + " was not recived in time!", device, TIMEOUT_MESSAGE_SENT, new Predicate<WebDriver>() {
                    public boolean apply(WebDriver t) {
                        return ElementUtils.getTextView(device, content) != null;
                    }
                });
    }

    public List<Message> getMessages(AndroidDriver device) {
        List<Message> result = new ArrayList<Message>();
        for (WebElement messageBlock : getMessageBlockElements(device)) {
            List<WebElement> textViews = messageBlock.findElements(By.className(ElementUtils.TEXT_VIEW));
            Message msg = new Message();
            msg.setText(textViews.get(0).getText());
            msg.setSent(textViews.get(1).getText());
            result.add(msg);
        }
        return result;
    }

    public void waitUntilSeen(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Seen was not shown in time!", device, TIMEOUT_SEEN, new Predicate<WebDriver>() {

            public boolean apply(WebDriver t) {
                return !ElementUtils.getTextViews(device).isEmpty()
                        && getSeen(device).contains(SEEN_TEXT);
            }
        });
    }

    public String getSeen(AndroidDriver device) {
        return ElementUtils.getLastTextView(device).getText();
    }

    public String getSeenTime(AndroidDriver device) {
        String wholeSeen = getSeen(device);
        return wholeSeen.substring(wholeSeen.indexOf(" ")).trim();
    }

    public List<WebElement> getMessageBlockElements(AndroidDriver device) {
        WebElement listView = ElementUtils.getListViews(device).get(0);
        List<WebElement> relativeLayouts = listView.findElements(By.className(ElementUtils.RELATIVE_LAYOUT));
        List<WebElement> actualMessageBlocks = new ArrayList();
        for (int i = 0; i < relativeLayouts.size(); i += 2) {
            actualMessageBlocks.add(relativeLayouts.get(i));
        }
        return actualMessageBlocks;
    }

    public WebElement getLastMessageBlockElement(AndroidDriver device) {
        List<WebElement> messages = getMessageBlockElements(device);
        return messages.get(messages.size() - 1);
    }

    public void forwardMessage(WebElement message, AndroidDriver device) {
        TouchAction action = new TouchAction(device);
        action.longPress(message).perform();
        ElementUtils.getTextView(device, FORWARD_MESSAGE).click();
    }
    
    public void goBackToContactList(AndroidDriver device) {
        ElementUtils.getImageButtons(device).get(0).click();
    }

    public static class Message {

        private String text;
        private String sent;

        public Message() {
        }

        public Message(String text, String sent) {
            this.text = text;
            this.sent = sent;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getSent() {
            return sent;
        }

        public void setSent(String sent) {
            this.sent = sent;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 83 * hash + (this.text != null ? this.text.hashCode() : 0);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Message other = (Message) obj;
            if ((this.text == null) ? (other.text != null) : !this.text.equals(other.text)) {
                return false;
            }
            return true;
        }

    }
}
