package net.phonex.tests.fragments.common;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import java.util.List;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jhuska
 */
public class HamburgerMenuFragment {

    private static final int TIMEOUT_SHOWN = 4000;
    
    private static final String HEADER = "Expires";
    
    private static final String SEND_BROADCAST_MSG = "Send broadcast message";
    private static final String MY_ACCOUNT = "My account";
    private static final String SETTINGS = "Settings";
    private static final String HELP = "Help";
    private static final String LOG_OUT = "Log out";
    
    public void show(AndroidDriver device) {
        ElementUtils.getImageButtons(device).get(0).click();
        waitUntilVisible(device);
    }
    
    public void tapSendBroadcastMsg(AndroidDriver device) {
        ElementUtils.getTextView(device, SEND_BROADCAST_MSG).click();
    }
    
    public void tapMyAccount(AndroidDriver device) {
        ElementUtils.getTextView(device, MY_ACCOUNT).click();
    }
    
    public void tapSettings(AndroidDriver device) {
        ElementUtils.getTextView(device, SETTINGS).click();
    }
    
    public void tapHelp(AndroidDriver device) {
        ElementUtils.getTextView(device, HELP).click();
    }
    
    public void tapLogOut(AndroidDriver device) {
        ElementUtils.getTextView(device, LOG_OUT).click();
    }
    
    public void waitUntilVisible(final AndroidDriver device) {
        WaitUtils.waitUntilPredicateHolds("Hamburger menu was not shown in time!", 
                device, TIMEOUT_SHOWN, new Predicate<WebDriver>() {

            public boolean apply(WebDriver t) {
                List<WebElement> textViews = ElementUtils.getTextViews(device);
                for(WebElement textView : textViews) {
                    if(textView.getText().contains(HEADER)) {
                        return true;
                    }
                }
                return false;
            }
        });
    }
}