package net.phonex.tests;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import static net.phonex.tests.AbstractTest.QA_LOGIN_DEVICE_2;
import net.phonex.tests.utils.AsyncUtils;
import org.junit.Before;

/**
 *
 * @author jhuska
 */
public class AbstractTestTwoDevices extends AbstractTest {

    private Future<String> secondDeviceFT = null;

    @Before
    public void set_up() throws Exception {
        secondDeviceFT = AsyncUtils.executeAsyncTask(new Callable<String>() {
            public String call() throws Exception {
                set_up_device2();
                loginIntoApp(device2, QA_LOGIN_DEVICE_2);
                waitUntilUserIsLoggedIn(device2);
                return "done";
            }
        });
        set_up_device1();
        loginIntoApp(device1, QA_LOGIN_DEVICE_1);
        waitUntilUserIsLoggedIn(device1);
        secondDeviceFT.get();
    }
}
