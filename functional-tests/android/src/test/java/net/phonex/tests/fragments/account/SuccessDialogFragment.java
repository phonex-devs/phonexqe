package net.phonex.tests.fragments.account;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import net.phonex.tests.utils.ElementUtils;
import net.phonex.tests.utils.WaitUtils;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author jhuska
 */
public class SuccessDialogFragment {

    private static final int TIMEOUT_TRIAL_GENERATED = 15000;
    
    private static final String SUCCESS_DIALOG_HEADER = "Success";
    
    /**
     * Waits till success dialog is present, and then clicks on the log in button.
     * @param driver
     * @return login for newly created license
     * @throws Exception 
     */
    public String logIn(final AndroidDriver driver) throws Exception {
         waitUntilSuccessDialogPresent(driver);
         String result = ElementUtils.getTextViews(driver).get(3).getText();
         ElementUtils.getLastTextView(driver).click();
         return result;
    }
    
    private void waitUntilSuccessDialogPresent(final AndroidDriver driver) {
        WaitUtils.waitUntilPredicateHolds("Success dialog was not shown in time!", 
                driver, TIMEOUT_TRIAL_GENERATED, 
                 new Predicate<WebDriver>() {
             public boolean apply(WebDriver t) {
                 return !ElementUtils.getTextViews(driver).isEmpty()
                         &&
                         (ElementUtils.getTextViews(driver).get(0).getText()
                                 .equals(SUCCESS_DIALOG_HEADER));
             }
         });
    }
}