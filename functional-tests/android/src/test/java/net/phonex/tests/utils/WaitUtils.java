package net.phonex.tests.utils;

import com.google.common.base.Predicate;
import io.appium.java_client.android.AndroidDriver;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author jhuska
 */
public class WaitUtils {

    public static void waitUntilElementIsPresent(AndroidDriver device,
            long timeouInMillisec, final WebElement... elements) {
        WebDriverWait wait = new WebDriverWait(device, timeouInMillisec / 1000);
        wait.pollingEvery(1, TimeUnit.SECONDS);
        wait.until(new Predicate<WebDriver>() {

            public boolean apply(WebDriver t) {
                boolean result = false;
                for (WebElement element : elements) {
                    if (element.isDisplayed()) {
                        result = true;
                        break;
                    }
                }
                return result;
            }
        });
    }

    public static void waitUntilPredicateHolds(String errorMsg, AndroidDriver device, long timeouInMillisec,
            Predicate<WebDriver> predicate) {
        new WebDriverWait(device, timeouInMillisec / 1000)
                .pollingEvery(200, TimeUnit.MILLISECONDS)
                .withMessage(System.lineSeparator() + errorMsg + System.lineSeparator())
                .until(predicate);
    }
}
