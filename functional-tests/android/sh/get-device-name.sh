#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
echo `grep -B1 "$1"\" $DIR/device-list.json | cut -d$'\n' -f1 |  cut -d':' -f1 | cut -d'"' -f2`
