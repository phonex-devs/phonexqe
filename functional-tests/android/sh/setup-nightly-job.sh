#!/bin/bash
COMMAND_TIMEOUT=180
export COMMAND_TIMEOUT

#PREPARING APK
wget -q http://localhost:8080/view/nightly_build/job/1-nightly-phonex-build/lastSuccessfulBuild/artifact/phonexAnd/app/build/outputs/apk/qe/app-debug-qe.zip
mkdir /tmp/apks
mv app-debug-qe.zip /tmp/apks
mkdir /tmp/apks/1
mkdir /tmp/apks/2
unzip /tmp/apks/app-debug-qe.zip -d /tmp/apks/1
cp /tmp/apks/1/app-debug.apk /tmp/apks/2

#PREPARING LOGS DIR
cd ${WORKSPACE}
export logs=build-${BUILD_NUMBER};
mkdir $logs;

#REINSTALL APK FLAG
echo ${REINSTALL_APP_ON_1_DEVICE}
echo ${REINSTALL_APP_ON_1_DEVICE}

REINSTALL_APP_ON_1_DEVICE_FLAG=""
REINSTALL_APP_ON_2_DEVICE_FLAG=""
if [ ${REINSTALL_APP_ON_1_DEVICE} = false ]; then
  REINSTALL_APP_ON_1_DEVICE_FLAG="--no-reset"
  export REINSTALL_APP_ON_1_DEVICE_FLAG
else
  REINSTALL_APP_ON_1_DEVICE_FLAG="--full-reset"
  export REINSTALL_APP_ON_1_DEVICE_FLAG
fi
if [ ${REINSTALL_APP_ON_2_DEVICE} = false ]; then
  REINSTALL_APP_ON_2_DEVICE_FLAG="--no-reset"
  export REINSTALL_APP_ON_2_DEVICE_FLAG
else
  REINSTALL_APP_ON_2_DEVICE_FLAG="--full-reset"
  export REINSTALL_APP_ON_2_DEVICE_FLAG
fi

#FIND DEVICES TO TEST ON
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
DEVICES=`bash $DIR/findTwoDevicesForTesting.sh`
DEVICE1_UDID=`echo $DEVICES | cut -d':' -f1`
DEVICE2_UDID=`echo $DEVICES | cut -d':' -f2`
DEVICE1_NAME=`bash $DIR/get-device-name.sh $DEVICE1_UDID`
DEVICE2_NAME=`bash $DIR/get-device-name.sh $DEVICE2_UDID`
echo "DEVICE_1_NAME=$DEVICE1_NAME" > env.properties
echo "DEVICE_2_NAME=$DEVICE2_NAME" >> env.properties

#START NODE AND LOGGING
node /home/jhuska/Downloads/appium/appium -p ${APPIUM_1_P} -bp ${APPIUM_1_BP} -U ${DEVICE1_UDID} --command-timeout ${COMMAND_TIMEOUT} ${REINSTALL_APP_ON_1_DEVICE_FLAG} > ${WORKSPACE}/$logs/appium-server1.log &

node /home/jhuska/Downloads/appium/appium -p ${APPIUM_2_P} -bp ${APPIUM_2_BP} -U ${DEVICE2_UDID} --command-timeout ${COMMAND_TIMEOUT} ${REINSTALL_APP_ON_2_DEVICE_FLAG} > ${WORKSPACE}/$logs/appium-server2.log &

sh/logcatSH/logcatOnDeviceCI.sh ${DEVICE1_UDID} ${WORKSPACE}/$logs
sh/logcatSH/logcatOnDeviceCI.sh ${DEVICE2_UDID} ${WORKSPACE}/$logs
