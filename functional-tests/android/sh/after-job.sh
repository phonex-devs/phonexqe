#!/bin/bash
cd ${WORKSPACE}
export logs=build-${BUILD_NUMBER};
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
DEVICE_UDID=`bash $DIR/get-device-udid.sh $1`
bash $DIR/pull-screenshots-and-make-video.sh $DEVICE_UDID $logs



