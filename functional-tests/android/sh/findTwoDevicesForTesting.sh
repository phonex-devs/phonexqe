#!/bin/bash
AVAILABLE_DEVICES=`adb devices`
DEVICE1=`echo $AVAILABLE_DEVICES | cut -d ' ' -f5`
DEVICE2=`echo $AVAILABLE_DEVICES | cut -d ' ' -f7`
if [[ -z "$DEVICE1" ]]; then
  echo "Not enough devices to test on! Missing both devices."
  exit -1
fi
if [[ -z "$DEVICE2" ]]; then
  echo "Not enough devices to test on! Missing second device."
  exit -1
fi

RESULT="$DEVICE1:$DEVICE2"
echo $RESULT
