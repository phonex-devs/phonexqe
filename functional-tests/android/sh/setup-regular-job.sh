#!/bin/bash
export COMMAND_TIMEOUT=180

wget -q http://localhost:8080/view/F_tests/job/phonex-build/lastSuccessfulBuild/artifact/phonexAnd/app/build/outputs/apk/qe/app-debug-qe.zip
mkdir /tmp/apks
mv app-debug-qe.zip /tmp/apks
mkdir /tmp/apks/1
mkdir /tmp/apks/2
unzip /tmp/apks/app-debug-qe.zip -d /tmp/apks/1
cp /tmp/apks/1/app-debug.apk /tmp/apks/2

cd ${WORKSPACE}
export logs=build-${BUILD_NUMBER};
mkdir $logs;

echo ${REINSTALL_APP_ON_1_DEVICE}
echo ${REINSTALL_APP_ON_1_DEVICE}

REINSTALL_APP_ON_1_DEVICE_FLAG=""
REINSTALL_APP_ON_2_DEVICE_FLAG=""
if [ ${REINSTALL_APP_ON_1_DEVICE} = false ]; then
  REINSTALL_APP_ON_1_DEVICE_FLAG="--no-reset"
  export REINSTALL_APP_ON_1_DEVICE_FLAG
fi
if [ ${REINSTALL_APP_ON_2_DEVICE} = false ]; then
  REINSTALL_APP_ON_2_DEVICE_FLAG="--no-reset"
  export REINSTALL_APP_ON_2_DEVICE_FLAG
fi

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
DEVICE1_UDID=`bash $DIR/get-device-udid.sh ${DEVICE_1_NAME}`
DEVICE2_UDID=`bash $DIR/get-device-udid.sh ${DEVICE_2_NAME}`

node /home/jhuska/Downloads/appium/appium -p ${APPIUM_1_P} -bp ${APPIUM_1_BP} -U ${DEVICE1_UDID} --command-timeout ${COMMAND_TIMEOUT} ${REINSTALL_APP_ON_1_DEVICE_FLAG} > ${WORKSPACE}/$logs/appium-server1.log &

node /home/jhuska/Downloads/appium/appium -p ${APPIUM_2_P} -bp ${APPIUM_2_BP} -U ${DEVICE2_UDID} --command-timeout ${COMMAND_TIMEOUT} ${REINSTALL_APP_ON_2_DEVICE_FLAG} > ${WORKSPACE}/$logs/appium-server2.log &

sh/logcatSH/logcatOnDeviceCI.sh ${DEVICE1_UDID} ${WORKSPACE}/$logs
sh/logcatSH/logcatOnDeviceCI.sh ${DEVICE2_UDID} ${WORKSPACE}/$logs
