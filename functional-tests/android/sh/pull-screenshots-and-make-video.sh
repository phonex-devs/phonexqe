#!/bin/bash
DIR_REMOTE=/sdcard/test-screenshots
mkdir "$2"/"$1"
cd "$2"/"$1"
adb -s $1 pull $DIR_REMOTE
#echo "Resizing screenshots to smaller size!"
#mogrify -resize 640x480 *.png
#echo "Converting to .gif."
#convert -delay 50 *.png "$1"-test.gif
#echo "Moving created .gif to upper folder."
#mv "$1"-test.gif ../ 
