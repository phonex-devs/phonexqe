#!/bin/bash
DIR=/sdcard/test-screenshots
adb -s $1 shell rm -r $DIR
adb -s $1 shell mkdir $DIR
for (( i=1; ; i++ ))
do
	name=`date +%s`
	adb -s $1 shell screencap -p "$DIR/$name.png"
done
