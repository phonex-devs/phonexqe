#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
echo `grep -A1 "$1"\" $DIR/device-list.json | grep "udid" | cut -d':' -f2 | cut -d',' -f1 | cut -d'"' -f2`;
